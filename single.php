<?php 
    if (have_posts()){
        the_post();

        if (in_category('events')) {
            get_template_part('single', 'news');
        }

        else {
            get_template_part('single', 'article');
        }
    }
?>