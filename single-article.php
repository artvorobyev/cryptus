<?php get_header();?>

        <div class="b-article" itemscope itemtype="http://schema.org/Article">
            <?php if (get_post_meta(get_the_ID(), 'is_partner', true) == '1'):?>
            <div class="b-article__source">
                <div class="b-article__source__left">Партнерский материал</div>
                <?php if (get_post_meta(get_the_ID(), 'partner_logo', true)):?>
                    <div class="b-article__source__logo">
                        <?php if (get_post_meta(get_the_ID(), 'partner_url', true)):?>
                        <a href="<?=get_post_meta(get_the_ID(), 'partner_url', true)?>">
                        <?php endif;?>
                            <img src="<?=wp_get_attachment_image_src(get_post_meta(get_the_ID(), 'partner_logo', true), '', true)[0]?>" alt="" title="" height="18">
                        <?php if (get_post_meta(get_the_ID(), 'partner_url', true)):?>
                        </a>
                        <?php endif;?>
                    </div>
                <?php endif;?>
                <div class="br"></div>
            </div>
        <?php endif;?>

            <meta itemprop="genre" content="<?php get_norm_cats_no_links(); ?>">
            <meta itemprop="datePublished" content="<?=get_the_date('Y-m-d')?>"/>
            <?php if (intval(get_post_meta(get_the_ID(), 'show_author', true))):?>
            <div itemprop="author" itemscope itemtype="http://schema.org/Person" style="display: none">
                <span itemprop="name"><?=get_the_author()?></span>
            </div>
            <?php endif;?>





            <div class="b-article__title"><h1 class="h1" itemprop="headline"><?php the_title();?></h1></div>
            <div class="b-article__pretext" itemprop="description"><?=get_subheading()?></div>

            <div class="b-article__info b-article__info_top ff-ss">
                <div class="b-article__info__left b-article__info__left_nopad">
                    <?=get_norm_date()?>, <?=get_the_time('G:i')?>
                    <span>&middot;</span>
                    <?php get_norm_cats();?>
                </div>
                <div class="b-article__info__right">
                    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                    <script src="//yastatic.net/share2/share.js"></script>
                    <div class="ya-share2" data-services="facebook,vkontakte,twitter,odnoklassniki" data-counter="" id='shareTop0'></div>
                </div>
                <div class="br"></div>
            </div>

            <?php if (intval(get_post_meta(get_the_ID(), 'is_banner', true))):?>
            <?php $banner_class = ' b-article__content_nobanner';?>
            <?php $banner_image_class = ' b-article__image_wide';?>

            <?php else:?>

            <div class="b-article__banner"><?=get_banner_by_slug('article','1v')?></div>
                
            <?php endif;?>

            <div class="b-article__content <?=$banner_class?>">

            <?php if (intval(get_post_meta(get_the_ID(), 'is_show_thumb', true)) != 1):?>

                <div class="b-article__image <?=$banner_image_class?>" itemscope itemtype="http://schema.org/ImageObject">
                    <img src="<?=get_thumb_url('cryptusHuge')?>" alt="<?=get_thumb_caption()?>" title="" width="700" height="467" itemprop="contentUrl" >
                    <div class="b-section__text ff-ss">
                    <?php
                    $meta = wp_prepare_attachment_for_js(get_post_thumbnail_id($post));
                    ?>

                        <div class='b-section__text__descr'><?=$meta['caption']?></div>

                    <?php if ($meta['description']): ?>
                       <div class='b-section__text__author'><?=$meta['description']?></div>
                    <?php endif;?>
                    </div>
                </div>
            <?php endif;?>

                <?php if (get_post_meta(get_the_ID(), 'lead', true)):?>

                <div class="b-article__important ff-ss">
                    <?=wpautop(get_post_meta(get_the_ID(), 'lead', true))?>
                </div>

                <?php endif;?>

                <div class="b-article__wrap">

                    <?php the_content(); ?>






                    <?php $meta = wp_prepare_attachment_for_js(get_post_thumbnail_id($post));?>

                    <?php if (intval(get_post_meta(get_the_ID(), 'is_show_thumb', true)) && $meta['description']):?>

                            
                            <div class="b-section__text ff-ss" style="border-bottom: none; padding: 30px 0 0;">

                               <div class='b-section__text__author'>Источник изображения в анонсе: <?=str_replace('Фото: ','',$meta['description'])?></div>
                           
                            </div>
                        
                    <?php endif;?>


                </div>

            </div>
            <div class="br"></div>

            <div class="b-article__info b-article__info_bottom b-article__info_tagged ff-ss">
                <div class="b-article__info__left">
                    <?php if (intval(get_post_meta(get_the_ID(), 'show_author', true))):?>
                    <?php if (count(explode(',',coauthors_posts_links(', ', ', ', '', '', false))) > 1):?>
                        <?= str_replace('author url fn', 'b-article__info__author',coauthors_posts_links(', ', ', ', '', '', false));?>

                    <?php else:?>
                    <a href="<?=get_author_posts_url( get_the_author_meta( 'ID' ))?>" class="b-article__info__author">
                    <span class="b-article__info__author__image">
                        <?php if (get_author_photo(get_the_author_meta( 'ID' ))):?>
                            <img src="<?=get_author_photo(get_the_author_meta( 'ID' ))?>" alt="" title="" width="50" height="50">
                        <?php endif;?>
                    </span><?=get_the_author()?></a>
                    
                    <?php endif;?>
                    <span>&middot;</span>
                    <?php endif; get_norm_cats();?>
                    <span>&middot;</span>
                    <?=get_norm_date()?>, <?=get_the_time('G:i')?>
                </div>
                <div class="b-article__info__right b-article__info__right_circles">
                    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                    <script src="//yastatic.net/share2/share.js"></script>
                    <div class="ya-share2" data-services="facebook,vkontakte,twitter,odnoklassniki" data-counter="" id="shareBottom0"></div>
                </div>
                <div class="br"></div>
            </div>



            <?php if (has_tag()):?>

            <div class="b-article__tags ff-ss">
                <?php get_norm_tags();?>
            </div>

            <?php endif;?>
        </div>

        <div class="spacer"></div>



        <?php get_template_part('readmore');?>



        <?php if (!intval(get_post_meta(get_the_ID(), 'is_banner', true))):?>

        <div class="spacer"></div>

        <div class="banner">
         <?=get_banner_by_slug('article','2h')?>

        </div>
        <?php endif;?>

        <div class="spacer js-last"></div>
        

    </div>

    <div id="forNextPosts"></div>
    


<?php get_footer();?>