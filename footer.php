
    <div class="b-foot ff-ss">
        <div class="l-wrap">
            <div class="b-foot__nav">
                <?php get_footer_menu(); ?>
            </div>
            <div class="b-foot__socials">
                Соцсети
                <a href="https://www.facebook.com/cryptus.world/" class="b-foot__socials_fb"><img class="svg" src="<?=get_template_directory_uri()?>/i/soc-fb.svg" alt="" title="" width="26" height="26"></a>

                <a href="https://vk.com/club143611099" class="b-foot__socials_vk"><img class="svg" src="<?=get_template_directory_uri()?>/i/soc-vk.svg" alt="" title="" width="26" height="26"></a>

                <a href="https://twitter.com/cryptus_world" class="b-foot__socials_tw"><img class="svg" src="<?=get_template_directory_uri()?>/i/soc-tw.svg" alt="" title="" width="26" height="26"></a>

                <a href="https://ok.ru/group/53551021818030" class="b-foot__socials_ok"><img class="svg" src="<?=get_template_directory_uri()?>/i/soc-ok.svg" alt="" title="" width="26" height="26"></a>
                
                <a href="https://www.instagram.com/cryptus.world/" class="b-foot__socials_ig"><img class="svg" src="<?=get_template_directory_uri()?>/i/soc-ig.svg" alt="" title="" width="26" height="26"></a>
            </div>
            <div class="br"></div>
            <div class="b-foot__partners">
                <!--<span class="b-foot__partners__title">Партнеры</span>
                <span class="b-foot__partners__links">
                    <a href="#"><img src="<?=get_template_directory_uri()?>/i/partners-1.png" alt="" title="" width="43" height="26"></a>
                    <a href="#"><img src="<?=get_template_directory_uri()?>/i/partners-2.png" alt="" title="" width="70" height="26"></a>
                    <a href="#"><img src="<?=get_template_directory_uri()?>/i/partners-3.png" alt="" title="" width="89" height="26"></a>
                    <a href="#"><img src="<?=get_template_directory_uri()?>/i/partners-4.png" alt="" title="" width="124" height="26"></a>-->
                </span>
            </div>
            <div class="b-foot__madeby">
                Сайт сделан <a href="http://brele.ru/" target="_blank">в Бреле</a> <sup class="ff-s">2017</sup>
            </div>
            <div class="br"></div>
        </div>
    </div>
</div>

<div id="fader"></div>
<div class="b-menu ff-ss">
    <div class="b-menu__wrap">
        <div class="b-menu__head">
            <div class="b-menu__close js-menu-close"><img class="svg" src="<?=get_template_directory_uri()?>/i/icon-close.svg" alt="" title="" width="20" height="20"></div>
            <div class="b-menu__logo"><a href="#"><img class="" src="<?=get_template_directory_uri()?>/i/logo.svg?v=2" alt="" title="" width="158" height="37"></a></div>
            <div class="br"></div>
        </div>
        <div class="b-menu__nav">
            
        <?php get_left_nav()?>
    </div>
</div>

<script>var ajaxUrl = "<?=admin_url();?>admin-ajax.php";</script>

<!--[if lt IE 9]>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<?php wp_footer();?>
</body>
</html>