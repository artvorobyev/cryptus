<?php

get_header();
global $wp_query;
$count = 0;
$real_count = 0;
$array2 = array(2,3,4,13,14,15);
$array5 = array(5,6,7,8, 16,17,18,19);
$all = $wp_query->post_count;
$borders = array('', '', 'bd-nor', '', '', 'bd-nor bd-noc', 'bd-nor', '', '', 'bd-nor bd-nol', 'bd-nor', '', '', 'bd-nor', '', '', 'bd-nor bd-noc', 'bd-nor', '', '');

while (have_posts()): the_post(); $count++; $real_count++;

if ($count == 20) $count = 1; 


    if ($count == 1):

?>



        <div class="b-items">
            <div class="b-category__top bd">

                <div class="b-category__top__banner"><?=get_banner_by_slug('category','1v')?></div>


                <div class="b-category__top__item">
                    <a href="<?=get_the_permalink()?>">
                        <span class="b-category__top__item__image">
                            <img src="<?=get_thumb_url('cryptusBig')?>" alt="<?=get_thumb_caption()?>" title="" width="860" height="573">
                        </span>
                        <span class="b-category__top__item__text">
                            <span class="b-category__top__item__text__title ff-ss"><?php the_title();?></span>
                            <?=get_subheading()?>
                            <span class="b-category__top__item__text__date date ff-ss"><?=get_norm_date(false)?></span>
                        </span>
                    </a>
                </div>
                <div class="br"></div>
            </div>
        </div>

        <div class="br"></div>

<?php  elseif (in_array($count, $array2) ):?>

        <?php if (in_array($count, array(2,13))):?>
        <div class="b-section nobd bd bd-0 bd-221 <?php if (in_array($all, $array2)) echo $borders[$all]?>">
        <?php endif;?>

            <a class="b-section__one <?php if (in_array($count, array(2,13))):?> pad-r<?php endif;?> <?php if (in_array($count, array(4,15))):?> pad-l<?php endif;?> <?php if (!in_array($count, array(4,15))):?>b-section__one_2<?php endif;?> js-height" data-height="section03" href="<?=get_the_permalink()?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusMedium')?>" alt="<?=get_thumb_caption()?>" title="" width="270" height="178"></span>
                <span class="b-section__one__title ff-ss"><?php the_title();?></span>
                <?=get_subheading()?>
                <span class="b-section__one__date date ff-ss <?php if (in_array($count, array(3,4,14,15))):?> pad-l <?php endif;?>"><?=get_norm_date(false)?></span>
            </a>

     <?php if (in_array($count, array(4,15)) || $real_count == $all):?>
            <div class="br"></div>
        </div>

        <div class="br"></div>
    <?php endif;?>

<?php  elseif (in_array($count, $array5) ):?>

        <?php if (in_array($count, array(5,16))): $class = 'pad-r';?>
        <div class="b-section bd bd-1111 bd-0 bd-tab50 nobd <?php if (in_array($all, $array5)) echo $borders[$all]?>">
            <div class="bd-1111__bd"></div>
        <?php endif;?>
        <?php if (in_array($count, array(6,17))): $class = 'tab-nopad tab-pad-l'; endif;?>
        <?php if (in_array($count, array(7,18))): $class = 'tab-nopad tab-pad-r'; endif;?>
        <?php if (in_array($count, array(8,19))): $class = 'pad-l'; endif;?>

            
            <a class="b-section__one b-section__one_tab3 <?=$class?> js-height" data-height="section08" href="<?=get_the_permalink()?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall')?>" alt="<?=get_thumb_caption()?>" title="" width="270" height="178"></span>
                <span class="b-section__one__title ff-ss"><?php the_title();?></span>
                <?=get_subheading()?>
                <span class="b-section__one__date date ff-ss <?php if (!in_array($count, array(5,16))):?>pad-l<?php endif;?>"><?=get_norm_date(false)?></span>
            </a>

        <?php if (in_array($count, array(8,19)) || $real_count == $all):?>

        
           
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <?php if ($count==8 || ( $all < 8 && $count == $all) ):?>

        <div class="banner">
        <?=get_banner_by_slug('category','2h')?>
        </div>

        <div class="spacer"></div>

        <?php endif;?>

        <?php endif;?>

    <?php elseif ($count == 9):?>


        <div class="b-section bd bd-0 bd-trans nobd <?php if (in_array($all, array(9,10,11,12))) echo $borders[$all]?>">


            <div class="b-section__banner">
                <?=get_banner_by_slug('category','2v')?>
            </div>


            <a class="b-section__one b-section__one_trans nopad pc-pad-r tab-nopad js-height" data-height="section05" href="<?=get_the_permalink()?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusBig')?>" alt="<?=get_thumb_caption()?>" title="" width="270" height="178"></span>
                <span class="b-section__one__text">
                    <span class="b-section__one__title ff-ss"><?php the_title();?>
                    <span class="b-section__one__date pad-l pc-pad-r tab-pad-l date ff-ss"><?=get_norm_date(false)?></span>
                </span>
            </a>
    <?php elseif (in_array($count, array(10,11,12))):?>
            <?php if ($count == 10): $class = 'pad-r pc-pad-l tab-nopad tab-pad-r'; $dateClass = 'pc-pad-l tab-nopad'; endif;?>
            <?php if ($count == 11): $class = 'pc-pad-r tab-pad-l'; $dateClass = 'pad-l pc-pad-r tab-nopad tab-pad-l'; endif;?>
            <?php if ($count == 12): $class = 'pad-l'; $dateClass = 'pad-l'; endif;?>

            <a class="b-section__one b-section__one_trans-other <?=$class?> js-height" data-height="section05" href="<?=get_the_permalink()?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall')?>" alt="<?=get_thumb_caption()?>" title="" width="270" height="178"></span>
                <span class="b-section__one__title ff-ss"><?php the_title();?>
                <span class="b-section__one__date <?=$dateClass?> date ff-ss"><?=get_norm_date(false)?></span>
            </a>

        <?php if ($count == 12 || $real_count == $all):?>
            <div class="br"></div>
        </div>

        <div class="br"></div>
        <?php endif;?>

    <?php endif; endwhile;?>

        

        
        <div class="spacer" style="clear:both"></div>

        <div id="loadHere"></div>

        

        <?php if ($wp_query->found_posts > $all):?>
            <a href="javascript:void(0)" id="loadCategory" class="btn" data-id="<?=get_queried_object_id()?>">Загрузить ещё</a><?php endif;?>

        <div class="spacer"></div>
    </div>
    </div>


<?php  get_footer();?>