<?php

include "functions/helpers.php";
include "functions/menus.php";
include "functions/meta-post.php";
include "functions/editor.php";
include "functions/post-gallery.php";
include "functions/gallery-page.php";
include "functions/news.php";
include "functions/search.php";
include "functions/author.php";
include "functions/banner.php";
include "functions/banner-page.php";
include "functions/opengraph.php";
include "functions/author_ajax.php";
include "functions/cat_ajax.php";
include "functions/nextPosts.php";
include "functions/quotes.php";
include "acf-options-page.php";

?>