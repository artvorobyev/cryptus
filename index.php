<?php get_header();?>
<?php

if (get_field('theme_show','options')) $mainTheme = get_field('theme_articles','options');
else $mainTheme = array();

$mainPosts = get_posts(
    array(
        'posts_per_page'=>3,
        'post_status'=>'publish',
        'post_type'=>'post',
        'meta_query' => array(
            array(
                'key' => 'is_main',
                'value' => '1'
            )
        ),
        'post__not_in' => prepare_exclude(array($mainTheme))
    )
);

$mainNews = get_posts(
    array(
        'posts_per_page'=>4,
        'post_status'=>'publish',
        'post_type'=>'post', 
        'meta_query' => array(
            array(
                'key' => 'main_news',
                'value' => '1'
            )
        ),
        'post__not_in' => prepare_exclude(array($mainTheme,$mainPosts))
    )
);

$days = get_posts(
    array(
        'posts_per_page'=>3,
        'post_status'=>'publish',
        'post_type'=>'post', 
        'meta_query' => array(
            array(
                'key' => 'is_day',
                'value' => '1'
            )
        ),
        'post__not_in' => prepare_exclude(array($mainTheme,$mainPosts,$mainNews))
    )
);

$play = get_posts(
    array(
        'posts_per_page'=>5,
        'post_status'=>'publish',
        'post_type'=>'post', 
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => array( 'play' ),
            )
        ),
        'post__not_in' => prepare_exclude(array($mainTheme,$mainPosts,$mainNews,$days))
    )
);

$stories = get_posts(
    array(
        'posts_per_page'=>3,
        'post_status'=>'publish',
        'post_type'=>'post', 
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => array( 'stories', 'columns' ),
            )
        ),
        'post__not_in' => prepare_exclude(array($mainTheme,$mainPosts,$mainNews,$days,$play))
    )
);

$exclude = prepare_exclude(array($mainTheme,$mainPosts,$mainNews,$days,$play,$stories));


?>
        <div class="b-items">
            <div class="b-category__top b-category__top_index bd">

               
                <div class="b-category__top__item">
                    <a href="<?=get_permalink($mainPosts[0])?>">
                        <span class="b-category__top__item__image">
                            <img src="<?=get_thumb_url('cryptusBig', $mainPosts[0])?>" alt="" title="" width="860" height="573">
                        </span>
                    </a>
                    <span class="b-category__top__item__text">
                        <a href="<?=get_permalink($mainPosts[0])?>" class="b-category__top__item__text__link">
                            <span class="b-category__top__item__text__title ff-ss"><?=$mainPosts[0]->post_title?></span>
                            <?=get_subheading($mainPosts[0]->ID)?>
                            <img src="<?=get_template_directory_uri()?>/images/hand.svg" alt="" width="58" height="23" class="svg b-category__top__item__text__hand">
                        </a>
                            <span class="b-category__top__item__text__date date ff-ss"><?php if (get_label($mainPosts[0]->ID)):?><?=get_label($mainPosts[0]->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$mainPosts[0])?></span>
                        </span>
                    </a>
                </div>

                <div class="b-category__top__leftcol">
                    <div class="b-section nobd">

                    <?php for ($i=1;$i<=2;$i++):?>

                        <div class="b-section__one b-section__one_index <?php if ($i == 1):?> nom <?php endif;?> nopad-l" data-height="section331">
                            <a href="<?=get_permalink($mainPosts[$i])?>" class="b-section__one__link">
                                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall', $mainPosts[$i])?>" alt="" title="" width="270" height="178"></span>
                                <span class="b-section__one__title ff-ss"><?=$mainPosts[$i]->post_title?></span>
                                <span><?=get_subheading($mainPosts[$i]->ID)?></span>
                            </a>
                            <span class="b-section__one__date pad-r date ff-ss"><?php if (get_label($mainPosts[$i]->ID)):?><?=get_label($mainPosts[$i]->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$mainPosts[$i])?></span>
                        </div>
                    <?php endfor;?>  
                       
                    </div>
                </div>

                <div class="b-category__top__rightcol">
                    <div class="b-section nobd">

                    <?php foreach ($mainNews as $k=>$itemNews):?>

                        <div class="b-section__one b-section__one_index pad-l <?php if ($k==0):?> nom <?php endif;?>">
                            <a href="<?=get_permalink($itemNews)?>" class="b-section__one__link">
                                <span class="b-section__one__title ff-ss"><?=$itemNews->post_title?></span>
                                <span><?=get_subheading($itemNews->ID)?></span>
                            </a>
                            <span class="b-section__one__date pad-l date ff-ss"><?php if (get_label($itemNews->ID)):?><?=get_label($itemNews->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$itemNews)?></span>
                        </div>
                    <?php endforeach;?>
                        
                    </div>
                </div>
            </div>
            
            <div class="br"></div>
        </div>

        

        <div class="br"></div>

        <div class="spacer"></div>

        <?php if (get_field('theme_show','options')):?>

        <div class="b-section b-section_top-image bd bd-3 nobd">
            <div class="b-section__title ff-ss"><?php the_field('theme_name', 'options'); ?></div>
            <div class="b-section__topic ff-ss"><?php the_field('theme_title', 'options'); ?></div>
            <?php

            $articles = get_field('theme_articles','options');
            foreach ($articles as $k=>$item):

            ?>


            <div class="b-section__one b-section__one_3 nom js-height <?php if ($k==0):?>pad-r <?php elseif($k==2):?> pad-l <?php endif;?>" data-height="section10">

                <a href="<?=get_permalink($item)?>" class="b-section__one__link">
                
                    <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusMedium', $item)?>" alt="" title="" width="270" height="178"></span>
                    <span class="b-section__one__title ff-ss"><?=$item->post_title?></span>
                    <span><?=get_subheading($item->ID)?></span>
                </a>
                <span class="b-section__one__date <?php if ($k != 0):?> pad-l <?php endif;?> date ff-ss"><?php if (get_label($item->ID)):?><?=get_label($item->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$item)?></span>
            </div>
            <?php endforeach;?>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <?php endif;?>

        <div class="b-section b-section_index bd bd-nol bd-rb bd-0">
            <div class="b-section__banner">
                <?=get_banner_by_slug('index','1v')?>
            </div>
            <div class="b-section__ov">
                




                <div class="b-list b-list_feed ff-ss">
                    <div class="b-list__view">
                        
                        <div class="b-list__section">
                            <div class="b-list__section__items">

                                <?php
                                    $feed_1 = get_posts(
                                        array(
                                            'posts_per_page'=>5,
                                            'post_status'=>'publish',
                                            'post_type'=>'post', 
                                            'post__not_in' => $exclude
                                        )
                                    );

                                    foreach ($feed_1 as $item):
                                ?>
                                
                                <div class="b-list__section__items__one">
                                    <div class="b-list__section__items__one__image">
                                        <a href="<?=get_permalink($item)?>" class="b-list__section__items__one__link">
                                            <img src="<?=get_thumb_url('cryptusSmall', $item)?>" alt="" title="" width="179" height="126">
                                        </a>
                                    </div>
                                    <div class="b-list__section__items__one__text">
                                        <a href="<?=get_permalink($item)?>" class="b-list__section__items__one__link">
                                            <div><?=$item->post_title?></div>
                                            <div class="b-list__section__items__one__descr ff-s">
                                                <?=get_subheading($item->ID)?>
                                            </div>
                                        </a>
                                        <div class="b-list__section__items__one__date date"><?php if (get_label($item->ID)):?><?=get_label($item->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$item)?></div>
                                    </div>
                                </div>
                                <?php $exclude[] = $item->ID;?>
                                <?php endforeach;?>
                                

                                <div class="fact-container">
                                    <div class="fotorama fotorama_fact" data-fact_type="quote" data-width="100%" data-arrows="always" data-loop="true" data-click="false">

                                        <?php
                                        $numbers = get_posts(
                                            array(
                                                'posts_per_page'=>-1,
                                                'post_status'=>'publish',
                                                'post_type'=>'quote'
                                            )
                                        );
                                        foreach ($numbers as $item):
                                            $meta = get_post_meta($item->ID);

                                            if (strlen($meta['fact_link'][0])>0):
                                        ?>
                                        <div data-href="<?=$meta['fact_link'][0]?>" class="fact fact_quote fact_link">
                                        <?php else:?>
                                        <div class="fact fact_quote">
                                        <?php endif;?>

                                            <div class="fact__left">

                                                <div class="fact__label date">цитата</div>
                                                <div class="fact__text ff-s"><?=$meta['fact_text'][0]?></div>
                                                <div class="fact__name ff-ss"><?=$meta['quote_author'][0]?></div>
                                                <div class="fact__job ff-s"><?=$meta['quote_job'][0]?></div>

                                            </div>
                                            <div class="fact__right">
                                                <div class="fact__image" style="background-image: url(<?=get_thumb_url('cryptusSmall',$item->ID)?>);"></div>
                                                <?php if (strlen($meta['fact_link'][0])>0): ?>
                                                <div class="fact__hand">
                                                    <img src="<?=get_template_directory_uri()?>/images/hand.svg" class="svg" alt="" width="58" height="23">
                                                </div>
                                                <?php endif;?>
                                            </div>
                                            
                                        </div>
                                        <?php unset($meta); endforeach;?>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <div class="br"></div>
                        </div>
                        <div class="br"></div>
                    </div>
                </div>








            </div>
            <div class="br"></div>
        </div>

        

        <div class="spacer"></div>


        <div class="b-section bd bd-top nom bd-3 nobd">
            <?php foreach($stories as $k=>$itemStories):?>
            
            <div class="b-section__one b-section__one_3 js-height <?php if ($k==0):?>pad-r <?php elseif($k==2):?> pad-l <?php endif;?>" data-height="section09">
                <a href="<?=get_permalink($itemStories)?>" class="b-section__one__link"> 
                    <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusMedium', $itemStories)?>" alt="" title="" width="270" height="178"></span>
                    <span class="b-section__one__title ff-ss"><?=$itemStories->post_title?></span>
                    <span><?=get_subheading($itemStories->ID)?></span>
                </a>
                <span class="b-section__one__date <?php if ($k != 0):?> pad-l <?php endif;?> date ff-ss"><?php if (get_label($itemStories->ID)):?><?=get_label($itemStories->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$itemStories)?></span>
            </div>


            
            <?php endforeach;?>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>


        <?php if (get_banner_by_slug('index','2h')):?>

        <div class="banner">
            <?=get_banner_by_slug('index','2h')?>
            
        </div>

        <div class="spacer"></div>

        <?php endif;?>




        <?php

            $images = explode(',', get_option('all_gallery_images'));
            $count = count($images);

            if ($count):

        ?>

        <div class="b-section nobd">
            <div class="b-section__title ff-ss"><a>Галереи</a></div>
            <div class="b-section__flex">
                <div class="b-section__gallery b-section__gallery--fotorama">
                    <div class="b-section__gallery__wrap">
                        
                        <div class="fotorama fotorama_gallery" data-width="920" data-maxwidth="100%" data-ratio="920/613" data-maxheight="613" data-margin="0" data-nav="thumbs" data-thumbheight="42" data-fit="cover" data-loop="true">
                            <?php
                                foreach ($images as $i=>$image) :

                                    $url[$i] = wp_get_attachment_image_src($image, 'cryptusHuge');
                                    $meta[$i] = wp_prepare_attachment_for_js($image);

                                    $caption[$i] = nl2br(htmlspecialchars($meta[$i]['caption']));
                                    $descr[$i] = nl2br(htmlspecialchars($meta[$i]['description']));


                                    if (!$descr[$i]) $descr[$i] = '&nbsp;'

                            ?>
                                    <img src="<?=$url[$i][0]?>" alt="" title="" width="700" height="467" data-cap="<?=$caption[$i]?>" data-description="<?=$descr[$i]?>">

                            <?php endforeach;?>
                        </div>
                        
                    </div>
                </div>
                <div class="b-section__text b-section__text--fotorama ff-ss nobd">
                    <div class='b-section__text__descr b-section__text__descr--fotorama js-fotorama-caption'>
                        <?=$caption[0]?>
                    </div>
                </div>

            </div>
            <div class='b-section__text__author b-section__text__author--fotorama ff-ss js-fotorama-description'>
                <?=$descr[0]?>
            </div>
        </div>

        <div class="br" style="border-bottom: 1px solid #bdc3cc;"></div>

        <div class="spacer"></div>

        <?php endif;?>







        <div class="b-section b-section_index bd bd-nol bd-rb bd-0">
            <div class="b-section__banner">
                <?=get_banner_by_slug('index','2v')?>
            </div>
            <div class="b-section__ov">
                




                <div class="b-list b-list_feed ff-ss">
                    <div class="b-list__view">
                        
                        <div class="b-list__section">
                            <div class="b-list__section__items">
                                <?php
                                    $feed_2 = get_posts(
                                        array(
                                            'posts_per_page'=>5,
                                            'post_status'=>'publish',
                                            'post_type'=>'post', 
                                            'post__not_in' => $exclude
                                        )
                                    );

                                    foreach ($feed_2 as $item):
                                ?>

                                <div class="b-list__section__items__one">
                                    <div class="b-list__section__items__one__image">
                                        <a href="<?=get_permalink($item)?>" class="b-list__section__items__one__link">
                                            <img src="<?=get_thumb_url('cryptusSmall', $item)?>" alt="" title="" width="179" height="126">
                                        </a>
                                    </div>
                                    <div class="b-list__section__items__one__text">
                                        <a href="<?=get_permalink($item)?>" class="b-list__section__items__one__link">
                                            <div><?=$item->post_title?></div>
                                            <div class="b-list__section__items__one__descr ff-s">
                                                <?=get_subheading($item->ID)?>
                                            </div>
                                        </a>
                                        <div class="b-list__section__items__one__date date"><?php if (get_label($item->ID)):?><?=get_label($item->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$item)?></div>
                                    </div>
                                </div>
                                <?php $exclude[] = $item->ID;?>
                                <?php endforeach;?>

                                <div class="fact-container">
                                <div class="fotorama fotorama_fact" data-fact_type="number" data-width="100%" data-arrows="always" data-loop="true" data-auto="false" data-click="false">

                                    <?php
                                    $numbers = get_posts(
                                        array(
                                            'posts_per_page'=>-1,
                                            'post_status'=>'publish',
                                            'post_type'=>'number'
                                        )
                                    );
                                    foreach ($numbers as $item):
                                        $meta = get_post_meta($item->ID);

                                        if (strlen($meta['fact_link'][0])>0):
                                    ?>
                                    <div class="fact fact_num fact_link" data-href="<?=$meta['fact_link'][0]?>">
                                    <?php else:?>
                                    <div class="fact fact_num">
                                    <?php endif;?>

                                        <div class="fact__label date">цифра</div>
                                        <div class="fact__number"><?=$item->post_title?></div>
                                        <div class="fact__descr ff-s"><?=$meta['fact_text'][0]?></div>
                                        <?php if (strlen($meta['fact_link'][0])>0):?>
                                        <div class="fact__hand">
                                            <img src="<?=get_template_directory_uri()?>/images/hand.svg" class="svg" alt="" width="58" height="23">
                                        </div>
                                        <?php endif;?>
                                    </div>

                                    <?php unset($meta); endforeach;?>

                                    
                                </div>

                                    
                                    
                                    
                                </div>
                            </div>
                            <div class="br"></div>
                        </div>
                        <div class="br"></div>
                    </div>
                </div>








            </div>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>


        <?php if (count($days)>0):?>
        <div class="b-section bd bd-3 bd-day nobd">
            <div class="b-section__title ff-ss">Избранные дни</div>

            <?php foreach ($days as $k=>$item):?>
            <?php $meta = get_post_meta($item->ID);?>
            <a class="b-section__one b-section__one_3 b-section__one_day <?php if ($k==0):?> pad-r <?php elseif($k==2):?> pad-l <?php endif;?> js-height" data-height="section33" href="<?=get_permalink($item)?>">
                <span class="b-section__one__image b-section__one__image_day"><img src="<?=get_thumb_url('cryptusMedium', $item)?>" alt="" title="" width="270" height="178"></span>
                <span class="b-section__one__day"><span class="b-section__one__day__num"><?=$meta['day_date'][0]?></span> <span class="b-section__one__day__date"><?=$meta['day_month'][0]?></span></span>
                <span class="b-section__one__title b-section__one__title_day ff-ss"><?=$item->post_title?></span>
                <span><?=get_subheading($item->ID)?></span>
            </a>
            <?php $meta = ''; endforeach;?>
            
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <?php endif;?>

        <?php if (get_banner_by_slug('index','3h')):?>

        <div class="banner">
            <?=get_banner_by_slug('index','3h')?>
            
        </div>

        <div class="spacer"></div>

        <?php endif;?>


        <div class="b-section b-section_play bd tab-nobd">
            
            <div class="b-section__one b-section__one_play b-section__one_v nobd pad-r">
                <a href="<?=get_permalink($play[0])?>" class="b-section__one__link">

                    <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusBig',$play[0])?>" alt="" title="" width="270" height="178"></span>
                    
                    <span class="b-section__one__title ff-ss"><?=$play[0]->post_title?></span>
                    <span class="b-section__one_v__text"><?=get_subheading($play[0]->ID)?></span>
                </a>
                <span class="b-section__one__date date ff-ss"><?php if (get_label($play[0]->ID)):?><?=get_label($play[0]->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$play[0])?></span>
                
            </div>
            <?php $playClass = array('','tab-nopad tab-pad-r', 'pad-l', 'nobd tab-nopad tab-pad-r', 'nobd pad-l');?>

            <?php foreach ($play as $k=>$item):?>

                <?php if ($k != 0):?>

                    <div class="b-section__one <?=$playClass[$k]?> js-height" data-height="section01">
                        <a href="<?=get_permalink($item)?>" class="b-section__one__link">
                            <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall', $item)?>" alt="" title="" width="270" height="178"></span>
                            <span class="b-section__one__title ff-ss"><?=$item->post_title?></span>
                            <?=get_subheading($item->ID)?>
                        </a>
                        <span class="b-section__one__date pad-l date ff-ss"><?php if (get_label($item->ID)):?><?=get_label($item->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$item)?></span>
                    </div>



                <?php endif;?>

            <?php endforeach;?>
            
            <div class="br"></div>
        </div>


        <div class="spacer mob-hide"></div>


        <div class="b-section b-section_index bd bd-nol bd-rb bd-0">
            <div class="b-section__banner">
                <?=get_banner_by_slug('index','3v')?>
            </div>
            <div class="b-section__ov">
                




                <div class="b-list b-list_feed ff-ss">
                    <div class="b-list__view">
                        
                        <div class="b-list__section">
                            <div class="b-list__section__items b-list__section__items_last-item-0" id="loadHere">
                                <?php
                                    $feed_3 = get_posts(
                                        array(
                                            'posts_per_page'=>5,
                                            'post_status'=>'publish',
                                            'post_type'=>'post', 
                                            'post__not_in' => $exclude
                                        )
                                    );

                                    foreach ($feed_3 as $item):
                                ?>

                                <div class="b-list__section__items__one">
                                    <div class="b-list__section__items__one__image">
                                        <a href="<?=get_permalink($item)?>" class="b-list__section__items__one__link">
                                            <img src="<?=get_thumb_url('cryptusSmall', $item)?>" alt="" title="" width="179" height="126">
                                        </a>
                                    </div>
                                    <div class="b-list__section__items__one__text">
                                        <a href="<?=get_permalink($item)?>" class="b-list__section__items__one__link">
                                            <div><?=$item->post_title?></div>
                                            <div class="b-list__section__items__one__descr ff-s">
                                                <?=get_subheading($item->ID)?>
                                            </div>
                                        </a>
                                        <div class="b-list__section__items__one__date date"><?php if (get_label($item->ID)):?><?=get_label($item->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$item)?></div>
                                    </div>
                                </div>
                                <?php $exclude[] = $item->ID;?>
                                <?php endforeach;?>
                                
                            </div>
                            <div class="br"></div>
                        </div>
                        <div class="br"></div>
                    </div>
                </div>








            </div>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <a class="btn" id="loadMainPage" data-exclude="<?=implode(',',$exclude)?>">Загрузить ещё</a>

        <div class="spacer"></div>


        <?php if (get_banner_by_slug('index','4h')):?>

        <div class="banner">
            <?=get_banner_by_slug('index','4h')?>
            
        </div>

        <div class="spacer"></div>

        <?php endif;?>
    </div>
    <?php get_footer();?>