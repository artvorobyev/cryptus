<?php 

    $readmore = get_field('read_more');

    if ($readmore) {
        $count = count($readmore);
    }

    else {
        $readmore = array();
        $count = 0;
    }

    

    $length = 4 - $count;

    if ($length != 0 ) {
        $cats = get_the_category(get_the_ID());
        foreach ($cats as $cat) {
            $cats_array[] = $cat->term_id;
        }

        $args = array(
            'posts_per_page' => $length,
            'cat' => $cats_array,
            'post__not_in' => array(get_the_ID()),
            'post_type' => 'post',
            'post_status' => 'publish'
        );

        $readmore2 = get_posts($args);

        $readmore = array_merge($readmore,$readmore2);
    }

?>
    <div class="b-section bd bd-1111 bd-tab50 nobd js-readmore">
            <div class="bd-1111__bd"></div>
            <div class="b-section__title ff-ss">Читайте также</div>
           
            
<?php
        $classes = array(
            'b-section__one b-section__one_tab3 pad-r js-height',
            'b-section__one b-section__one_tab3 tab-nopad tab-pad-l js-height',
            'b-section__one b-section__one_tab3 tab-nopad tab-pad-r js-height',
            'b-section__one b-section__one_tab3 pad-l js-height'
        );

        foreach( $readmore as $key=>$item): 
            if ($key != 0) $date_class[$key] = ' pad-l '
?>

             <a class="<?=$classes[$key]?>" data-height="section08" href="<?=get_permalink($item)?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall', $item)?>" alt="<?=get_thumb_caption($item)?>" title="" width="270" height="178"></span>
                <span class="b-section__one__title ff-ss"><?=$item->post_title?></span>
                <?=get_subheading($item->ID)?>
                <span class="b-section__one__date date ff-ss <?=$date_class[$key]?>"><?=get_norm_date(false, $item)?></span>
            </a>

        
<?php 
        endforeach;
?>

            <div class="br"></div>
        </div>

  
