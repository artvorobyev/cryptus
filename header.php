<?php
    /*if (is_single()) {
        $LastModified_unix = get_the_modified_date('U'); // время последнего изменения страницы
        $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
        $IfModifiedSince = false;
        if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
            $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));  
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
            $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
        if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
            exit;
        }
        header('Last-Modified: '. $LastModified);
    }*/
?>

<!DOCTYPE html>
<html>
<head lang="ru">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6027931381840634",
    enable_page_level_ads: true
  });
</script>
    <meta charset="UTF-8">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93142619-1', 'auto');
  ga('send', 'pageview');

</script>

<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '7d67055540f5b393e7108b9b792540ef7f528233');
</script>
    <meta charset="UTF-8">

    <meta name="format-detection" content="telephone=no">
    <meta name="mobileoptimized" content="0">
    <meta name="viewport" content="width=device-width">


    <link rel="apple-touch-icon" sizes="180x180" href="<?=get_template_directory_uri()?>/i/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?=get_template_directory_uri()?>/i/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?=get_template_directory_uri()?>/i/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?=get_template_directory_uri()?>/i/favicons/manifest.json">
    <link rel="mask-icon" href="<?=get_template_directory_uri()?>/i/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="Cryptus">
    <meta name="application-name" content="Cryptus">
    <meta name="msapplication-TileColor" content="#172a45">
    <meta name="msapplication-TileImage" content="<?=get_template_directory_uri()?>/i/favicons/mstile-144x144.png">
    <meta name="theme-color" content="#c0d2db">



    <title id="header-title">
        <?php
          global $page, $paged;
          wp_title( '—', true, 'right' );
          bloginfo( 'name' );
          $site_description = get_bloginfo( 'description', 'display' );
        ?>
    </title>
    
    <?php if (is_single()):?>
        <meta id="header-description" name="description" content="<?=get_subheading();?>" />

        <meta id="og_title" property="og:title" content="<?php the_title();?>" />
        <meta id="og_description" property="og:description" content="<?=get_subheading();?>" />
        <meta property="og:type" content="article" />
        <meta id="og_url" property="og:url" content="<?php the_permalink();?>" />
        
        <?php
            $ogImage = get_post_meta($post->ID, 'ogImage', true);
            if ($ogImage) $url = wp_get_attachment_image_src($ogImage, false, true);
            else $url = array('');
        ?>

        <meta id="og_image" property="og:image" content="<?=$url[0]?>" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta property="og:image:type" content="<?=getimagesize($url[0])['mime']?>" />

    <?php else:?>
        
        <meta name="description" content="<?php bloginfo( 'name' ); echo " — $site_description"; ?>" />


        <?php if (is_home()):?>

            <meta property="og:title" content="<?php bloginfo( 'name' );?>" />
            <meta name="og:description" content="<?php echo " — $site_description"; ?>" />

        <?php else:?>

            <meta property="og:title" content="<?php wp_title( '', true, 'right' );?>" />
            <meta name="og:description" content="<?php bloginfo( 'name' ); echo " — $site_description"; ?>" />
        
        <?php endif;?>

    <?php endif;?>
    
    <meta property="og:locale" content="ru_Ru" />
    <meta property="og:site_name" content="<?php bloginfo( 'name' );?>" />

    


    <script type="text/javascript" src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri()?>/js/jquery.tinyscrollbar.min.js"></script>

    <script type="text/javascript" src="<?=get_template_directory_uri()?>/js/fotorama.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri()?>/js/site.js?v=<?=get_file_ver('/js/site.js')?>"></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">

    <link href="<?=get_template_directory_uri()?>/css/style.css?v=<?=get_file_ver('/css/style.css')?>" rel="stylesheet">

    <?php wp_head();?>


    <style>

        .l-wrap--article {
            background: #fff;
        }

        .loading {
            width: 100%;
            position: fixed;
            z-index:-10;
            top:100px;
            opacity: 1;
        }

        .loading.showed {
            position: relative;
            z-index:1;
            background: #fff;
        }

        .loading.opacity {
            opacity: 0;
        }

        .loading-shadow {
            content:'';
            z-index:0;
            position: relative;
            width:100%;
            height:100vh;
            display: block;
            background: #000;
            opacity: 0.9;
            padding-bottom: 100px;
            box-sizing: content-box;

        }

        .topBanner {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .bd-221 {
            overflow:visible;
        }

        
    </style>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1258343404275977'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1258343404275977&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1258343404275977', {
em: 'insert_email_variable,'
});
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1258343404275977&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
    
</head>
<body>
    
<div class="l-page">
    <?php if (is_single() ): 
        if (in_category('events') && get_banner_by_slug('event_single','1h')):?>
        <div class="banner" id="topBanner">
            <?=get_banner_by_slug('event_single','1h')?>
        </div>
        <?php
        elseif (!intval(get_post_meta($post->ID, 'is_banner', true)) && get_banner_by_slug('article','1h')):?>
        <div class="banner" id="topBanner">
            <?=get_banner_by_slug('article','1h')?>
        </div>
        <?php endif;?>
    <?php elseif(is_home() && get_banner_by_slug('index','1h')):?>
        <div class="banner" id="topBanner">
            <?=get_banner_by_slug('index','1h')?>
        </div>
     <?php elseif(is_category()):?>
        <?php if (is_category('events') && get_banner_by_slug('event_archive','1h')):?>
            <div class="banner" id="topBanner">
                <?=get_banner_by_slug('event_archive','1h')?>
            </div>
        <?php elseif (get_banner_by_slug('category','1h')):?>
             <div class="banner" id="topBanner">
                <?=get_banner_by_slug('category','1h')?>
            </div>

        <?php endif;?>
    <?php elseif( (is_author() || is_tag() || is_archive() ) && get_banner_by_slug('other','1h') ):?>
        <div class="banner" id="topBanner">
            <?=get_banner_by_slug('other','1h')?>
        </div>
    <?php elseif(is_search() && get_banner_by_slug('search','1h')):?>
        <div class="banner" id="topBanner">
            <?=get_banner_by_slug('search','1h')?>
        </div>

    <?php endif;?>

    <div class="l-wrap <?php if(is_single()):?> l-wrap--current l-wrap--article <?php endif;?>" <?php if(is_single()):?> data-post="<?=get_the_ID()?>"<?php endif;?>>
        <div class="b-head">
            <div class="b-head__search">
                <span class="js-search-open"><img class="svg" src="<?=get_template_directory_uri()?>/i/icon-search.svg" alt="" title="" width="20" height="20"></span>
                <div class="b-head__search__form js-search-form">
                    <form action="/" method="get">
                        <div class="b-head__search__form__field b-input"><input class="js-search-field" type="text" name="s"></div>
                        <div class="b-head__search__form__submit"><span class="js-submit"><img class="svg" src="<?=get_template_directory_uri()?>/i/icon-search.svg" alt="" title="" width="20" height="20"></span></div>
                        <div class="b-head__search__form__close"><span class="js-search-close"><img class="svg" src="<?=get_template_directory_uri()?>/i/icon-close.svg" alt="" title="" width="20" height="20"></span></div>
                    </form>
                </div>
            </div>
            <div class="b-head__menu"><span class="js-menu-open"><img class="svg" src="<?=get_template_directory_uri()?>/i/icon-menu.svg" alt="" title="" width="20" height="20"></span></div>
            <div class="b-head__logo ff-ss"><a href="/"><img class="svg" src="<?=get_template_directory_uri()?>/i/logo.svg" alt="" title="" width="158" height="37"></a><span>Развлекаем знаниями</span></div>
            <div class="br"></div>
            <div class="b-head__categories ff-ss">
                <?php get_header_nav(); ?>
            </div>
        </div>