<?php

function og_custom_boxes() {
		add_meta_box( 'og_image', 'Картинка для шэринга', 'og_image_meta_box_callback', 'post' );	
		
}

add_action('add_meta_boxes', 'og_custom_boxes');

function og_image_meta_box_callback() {
		global $post;
	?>
		<div style="height: 0px; overflow: hidden; position: relative;">
			<div id="ogDiv">
				<div id="ogLogo">
					<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 165 36" style="width:180px; height:42px;"><defs><style>.cls-1{fill:rgb(192,210,219);}</style></defs><title>Artboard 13</title><path class="cls-1" d="M13.58,1.35A11,11,0,0,1,17.35,2a9.52,9.52,0,0,1,1.35.6,9.42,9.42,0,0,1,.93.58V2H25v8.86H19.3A5.34,5.34,0,0,0,19,9a3.27,3.27,0,0,0-.71-1.19,2.56,2.56,0,0,0-1-.65A4.19,4.19,0,0,0,16,7a3.43,3.43,0,0,0-1.64.37,2.74,2.74,0,0,0-1.11,1.18,7.11,7.11,0,0,0-.65,2.16A20.81,20.81,0,0,0,12.35,14a19.19,19.19,0,0,0,.24,3.29,6.53,6.53,0,0,0,.71,2.14,2.89,2.89,0,0,0,1.23,1.19,4,4,0,0,0,1.79.37,3.5,3.5,0,0,0,1.35-.27,3.12,3.12,0,0,0,1.15-.82,4,4,0,0,0,.78-1.38,6.2,6.2,0,0,0,.28-2h5.65a11.6,11.6,0,0,1-.65,3.9,8.51,8.51,0,0,1-2,3.22,9.84,9.84,0,0,1-3.51,2.19,14.16,14.16,0,0,1-5.05.81,17,17,0,0,1-5-.71,11,11,0,0,1-4.09-2.24,10.62,10.62,0,0,1-2.76-3.92,14.73,14.73,0,0,1-1-5.76,14.52,14.52,0,0,1,1-5.76A11.08,11.08,0,0,1,5.19,4.31,10.75,10.75,0,0,1,9.07,2.07,14.4,14.4,0,0,1,13.58,1.35Z"/><path class="cls-1" d="M37.84,8l-.28,4.47h.28a7.7,7.7,0,0,1,1.89-3.88A4.66,4.66,0,0,1,43.17,7.3a5.3,5.3,0,0,1,2.15.4,3.9,3.9,0,0,1,1.45,1.06,4.14,4.14,0,0,1,.8,1.54,6.63,6.63,0,0,1,.24,1.8,5.16,5.16,0,0,1-.23,1.51A3.81,3.81,0,0,1,46.86,15a3.67,3.67,0,0,1-1.3,1,4.72,4.72,0,0,1-2,.36A3.72,3.72,0,0,1,41,15.45a2.58,2.58,0,0,1-.93-2,3.14,3.14,0,0,1,.13-1,3.84,3.84,0,0,1,.27-.65c.09-.17.15-.29.19-.37s0-.11-.11-.11-.47.26-.74.78a5.67,5.67,0,0,0-.4,2.45v3.72a2,2,0,0,0,.3,1.23,1.46,1.46,0,0,0,1.16.37h1v6H28.19v-6h1a1.23,1.23,0,0,0,1-.43,1.59,1.59,0,0,0,.35-1V15.36a1.1,1.1,0,0,0-.45-1,1.93,1.93,0,0,0-1-.27h-.86V8Z"/><path class="cls-1" d="M63.63,27.27q-.55,1.2-1.14,2.57A10.5,10.5,0,0,1,61,32.36a7.54,7.54,0,0,1-2.44,1.93,8.59,8.59,0,0,1-3.9.76,9.22,9.22,0,0,1-3-.43,6.08,6.08,0,0,1-2.1-1.16,4.59,4.59,0,0,1-1.23-1.68,5,5,0,0,1-.4-1.94,4,4,0,0,1,1.21-3.09,4.24,4.24,0,0,1,3-1.13,4.48,4.48,0,0,1,1.77.31,3.46,3.46,0,0,1,1.17.8,2.74,2.74,0,0,1,.63,1.07,3.8,3.8,0,0,1,.18,1.11,2.38,2.38,0,0,1-2,2.57.42.42,0,0,0,.21.1,2.09,2.09,0,0,0,.8.1,1.85,1.85,0,0,0,1-.28,3.76,3.76,0,0,0,.81-.7,5.15,5.15,0,0,0,.63-.89,7.81,7.81,0,0,0,.43-.89l0-.1L51,15.45a3.46,3.46,0,0,0-.35-.63,1.61,1.61,0,0,0-.45-.43,2,2,0,0,0-.67-.25,5.12,5.12,0,0,0-1-.08V8h13v6a2.06,2.06,0,0,0-1.34.3q-.32.3-.06.93l2,4.19L64,15.29a.76.76,0,0,0-.06-.9,1.9,1.9,0,0,0-1.34-.33V8h9.05v6a3.09,3.09,0,0,0-1.68.33,2.59,2.59,0,0,0-.82,1.06Z"/><path class="cls-1" d="M89.85,26.47A14.61,14.61,0,0,1,88,26.36a9.83,9.83,0,0,1-1.48-.3,9.94,9.94,0,0,1-1.27-.46L84.05,25v1.59a2,2,0,0,0,.3,1.23,1.48,1.48,0,0,0,1.18.36h1v6H72.81v-6h1a1.23,1.23,0,0,0,1-.43,1.59,1.59,0,0,0,.35-1V15.36a1.1,1.1,0,0,0-.46-1,2,2,0,0,0-1-.27h-.86V8H83.35v3.33h.28a5.79,5.79,0,0,1,2.13-2.79,7,7,0,0,1,4.19-1.11,7.59,7.59,0,0,1,6,2.41q2.15,2.41,2.15,7.08T95.87,24A7.72,7.72,0,0,1,89.85,26.47Zm-1-9.36a13.26,13.26,0,0,0-.1-1.64,4.64,4.64,0,0,0-.37-1.36,2.32,2.32,0,0,0-.71-.92,1.93,1.93,0,0,0-1.18-.33A2,2,0,0,0,84.52,14a4.55,4.55,0,0,0-.36,1.32A11.8,11.8,0,0,0,84.05,17v4a1.89,1.89,0,0,0,1,.67,4.34,4.34,0,0,0,1.38.25,1.66,1.66,0,0,0,1.18-.42,2.92,2.92,0,0,0,.71-1.1,6.06,6.06,0,0,0,.37-1.54A15.47,15.47,0,0,0,88.81,17.11Z"/><path class="cls-1" d="M110.78,2V8h4.86v6h-4.86v5a4.38,4.38,0,0,0,.3,1.75,1.22,1.22,0,0,0,1.24.68,1.53,1.53,0,0,0,.84-.2,1.51,1.51,0,0,0,.5-.54,2,2,0,0,0,.24-.76,8.33,8.33,0,0,0,0-.93V16.62h1.87v2.62a12.38,12.38,0,0,1-.33,2.94,5.91,5.91,0,0,1-1.14,2.36,5.53,5.53,0,0,1-2.19,1.58,9,9,0,0,1-3.48.58,9.92,9.92,0,0,1-3.7-.57,4.7,4.7,0,0,1-2.1-1.56,5.43,5.43,0,0,1-.93-2.36,18.75,18.75,0,0,1-.22-3V14.06H99.18V8.7a5,5,0,0,0,2.27-.53,5.46,5.46,0,0,0,1.8-1.46,7,7,0,0,0,1.17-2.14A7.83,7.83,0,0,0,104.83,2Z"/><path class="cls-1" d="M140.53,18.61a1.1,1.1,0,0,0,.45,1,2,2,0,0,0,1,.26h.86v6H133.2V21.48h-.26a5.59,5.59,0,0,1-2,3.77,7.77,7.77,0,0,1-4.83,1.28,8.83,8.83,0,0,1-3.62-.63,5.68,5.68,0,0,1-2.16-1.6,5.53,5.53,0,0,1-1-2.1,8.69,8.69,0,0,1-.29-2.12V15.65a2,2,0,0,0-.3-1.23,1.46,1.46,0,0,0-1.16-.37h-1V8h11.36V18.28a2.9,2.9,0,0,0,.15,1,1.43,1.43,0,0,0,.38.59,1.18,1.18,0,0,0,.54.27,2.6,2.6,0,0,0,.56.07,1.94,1.94,0,0,0,1.58-.62,2.69,2.69,0,0,0,.51-1.76V15.65a2,2,0,0,0-.3-1.23,1.46,1.46,0,0,0-1.16-.37h-1V8h11.34Z"/><path class="cls-1" d="M163.29,14h-4.47a4,4,0,0,0-1.46-1.85,3.59,3.59,0,0,0-2.06-.68,2.18,2.18,0,0,0-1.16.27.89.89,0,0,0-.43.8,1.07,1.07,0,0,0,.75,1,10.36,10.36,0,0,0,1.88.59q1.13.26,2.44.63a9.74,9.74,0,0,1,2.45,1,5.78,5.78,0,0,1,1.88,1.84,5.51,5.51,0,0,1,.75,3,4.88,4.88,0,0,1-.65,2.55A5.24,5.24,0,0,1,161.41,25a8.7,8.7,0,0,1-2.66,1,14.78,14.78,0,0,1-3.3.35A15.31,15.31,0,0,1,152.2,26a10.27,10.27,0,0,1-2.93-1.13L149.11,26h-4.29V19.52h4.45a5.36,5.36,0,0,0,1.6,2,3.51,3.51,0,0,0,2.14.71,1.94,1.94,0,0,0,1.28-.33,1,1,0,0,0,.38-.74,1.13,1.13,0,0,0-.75-1,9.89,9.89,0,0,0-1.84-.65l-2.41-.63a9.12,9.12,0,0,1-2.41-1A5.81,5.81,0,0,1,145.4,16a5.29,5.29,0,0,1-.75-3,4.93,4.93,0,0,1,.59-2.4,5.21,5.21,0,0,1,1.7-1.84,8.51,8.51,0,0,1,2.6-1.15,12.74,12.74,0,0,1,3.28-.4,13.38,13.38,0,0,1,3.6.45,8.56,8.56,0,0,1,2.4,1L159,7.57h4.31Z"/></svg>
				</div>
				<div id="ogText">
				</div>
			</div>
		</div>
		<div id="ogAppend">
			<?php
				if (get_post_meta($post->ID, 'ogImage', true)):
					$url = wp_get_attachment_image_src(get_post_meta($post->ID, 'ogImage', true), false, true);
			?>
					<img src="<?=$url[0]?>"/>
			<?php endif;?>
		</div>
		<br/>
		<!--<span class="button" id="ogGenerate">Сгенерировать новую картинку</span>-->
		<span id="ogLoading"><img src="/wp-admin/images/loading.gif"/></span>
		<input type="hidden" name="ogImage" id="ogImage" value="<?=get_post_meta($post->ID, 'ogImage', true)?>"/>


		<script>var ajaxUrl = "<?=admin_url();?>admin-ajax.php"; var id = "<?=get_the_ID()?>";</script>
	<?php
}


function og_image_save_postdata( $post_id ) {
	// проверяем, если это автосохранение ничего не делаем с данными нашей формы.
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return $post_id;

	if (isset($_REQUEST['ogImage'])):
		update_post_meta( $post_id, 'ogImage', $_POST['ogImage'] );	
	endif;
}

add_action( 'save_post', 'og_image_save_postdata' );

function og_admin_scripts() {
	global $post;

    if ( $post->post_type == 'post' ) {

          wp_enqueue_media();

          wp_register_script('html2canvas', get_template_directory_uri().'/functions/html2canvas.js', array('jquery','media-upload','thickbox'), false, true);
          wp_enqueue_script('html2canvas');

          wp_register_script('canvg', get_template_directory_uri().'/functions/canvg.js', array('jquery','media-upload','thickbox'), false, true);
          wp_enqueue_script('canvg');

          wp_register_script('base64', get_template_directory_uri().'/functions/base64.js', array('jquery','media-upload','thickbox'), false, true);
          wp_enqueue_script('base64');
          
          wp_register_script('opengraph', get_template_directory_uri().'/functions/opengraph.js', array('jquery','media-upload','thickbox'), 10, true);
          wp_enqueue_script('opengraph');

          

    }  
    
}
add_action('admin_enqueue_scripts', 'og_admin_scripts');

function og_admin_styles() {

    
    wp_register_style('opengraph', get_template_directory_uri().'/functions/opengraph.css');
    wp_enqueue_style('opengraph');
}

add_action('admin_print_styles', 'og_admin_styles');


function ogImageCreate() {
	$based64Image=substr($_POST['image'], strpos($_POST['image'], ',')+1);

    $image = imagecreatefromstring(base64_decode($based64Image));

    $dir = wp_upload_dir();

    if ($image != false) {
        $fileName = $dir['path'] . '/post_'. $_POST['id'] . '_' . time().'.png';

        imagepng($image, $fileName);

        
        	$filetype = wp_check_filetype( basename( $fileName ), null );
        	$attachment = array(
				'guid'           => $dir['url'] . '/' . basename( $fileName ), 
				'post_mime_type' => $filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $fileName ) ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			$attach_id = wp_insert_attachment( $attachment, $fileName, intval($_POST['id']) );

			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			 
			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $fileName );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			$url = wp_get_attachment_image_src($attach_id, false, true);

			$send['url'] = $url[0];
			$send['id'] = $attach_id;

			wp_send_json($send);
        
    }
    else {
			echo 'fail';
    }


}

add_action('wp_ajax_nopriv_ogImageCreate', 'ogImageCreate'); 
add_action('wp_ajax_ogImageCreate', 'ogImageCreate');



function getThumbnailUrlById() {
	$id = $_POST['thumbnailId'];

	if ($id) {
		$thumb_url = wp_get_attachment_image_src($id, false, true);
    	$url = $thumb_url[0];
    	wp_send_json($url);
	}
}

add_action('wp_ajax_nopriv_getThumbnailUrlById', 'getThumbnailUrlById'); 
add_action('wp_ajax_getThumbnailUrlById', 'getThumbnailUrlById');


?>