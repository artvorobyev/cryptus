<?php

function theme_settings_page()
{

        if (isset($_GET['settings-updated'])) {
            echo '<div id="message" class="updated fade"><p><strong>Изменения сохранены</strong></p></div>';
        }

    ?>
	    <div class="wrap">
	    
	    <h1>Галереи</h1>
	    <form method="post" action="options.php">
	        <?php
                settings_fields("section_gallery");
                do_settings_sections("theme-options");    
                $images = explode(',', get_option('all_gallery_images'));
                $count = count($images);
              ?>
              <div class="gallery__container js-sortable">
              <?php
              if ($count):
                foreach ($images as $i=>$image) :
                    $url[$i] = wp_get_attachment_image_src($image);
              ?>
                <div class="gallery__point" id="<?=$i?>">
                    <input type="hidden" class="gallery__id" name="gallery__id<?=$i?>" id="gallery__id<?=$i?>" value="<?=$image?>" />
                    <img src="<?=$url[$i][0]?>" class="gallery__image"/>
                    <span class="gallery__delete" data-delete="<?=$i?>">×</span>

                </div>
            <?php
            endforeach;
          endif;
          ?>
          
          </div>
          <a href="javascript:void(0)" class="gallery__add">Добавить изображение</a>
          <input type="hidden" name="gallery__count" id="gallery__count" value="<?=$count?>"/>

          <?php submit_button(); ?>
                   
	    </form>
		</div>
	<?php
}

function add_theme_menu_item()
{
	add_menu_page("Галереи", "Галереи", "edit_others_posts", "theme-panel", "theme_settings_page", 'dashicons-format-image', 10);
}

add_action("admin_menu", "add_theme_menu_item");

function display_all_gallery_images()
{
	?>
    	<input type="hidden" name="all_gallery_images" id="all_gallery_images" value="<?php echo get_option('all_gallery_images'); ?>"/>
    <?php
}



function display_theme_panel_fields()
{
	add_settings_section("section_gallery", "Настройки галереи", null, "theme-options");
	
	add_settings_field("all_gallery_images", " ", "display_all_gallery_images", "theme-options", "section_gallery");

    register_setting("section_gallery", "all_gallery_images");
}


add_action("admin_init", "display_theme_panel_fields");


function page_gallery_admin_scripts() {

    if ( get_current_screen()->base == 'toplevel_page_theme-panel' ) {

          wp_enqueue_media();
          
          wp_register_script('page-gallery', get_template_directory_uri().'/functions/gallery-page.js', array('jquery','media-upload','thickbox'), false, true);
          wp_enqueue_script('page-gallery');

    }  
    
}
add_action('admin_enqueue_scripts', 'page_gallery_admin_scripts');

function page_gallery_admin_styles() {

    
    wp_register_style('page-gallery', get_template_directory_uri().'/functions/gallery-page.css');
    wp_enqueue_style('page-gallery');
}

add_action('admin_print_styles', 'page_gallery_admin_styles');
?>