<?php

function author_ajax() {
	$count = 0;
	if ($_POST['type'] == 'author')
		$query = new wp_query(array('post_status'=>'publish', 'paged' => $_POST['page'], 'author' => $_POST['id'] ));

	elseif ($_POST['type'] == 'tag')
		$query = new wp_query(array('post_status'=>'publish', 'paged' => intval($_POST['page']), 'tag_id' => intval($_POST['id']) ));

	elseif ($_POST['type'] == 'year')
		$query = new wp_query(array('post_status'=>'publish', 'paged' => $_POST['page'], 'year' => $_POST['id']));

	elseif ($_POST['type'] == 'month') {
		$date = explode('.', $_POST['id']);
		$query = new wp_query(array('post_status'=>'publish', 'paged' => $_POST['page'], 'year' => $date[1], 'monthnum' => $date[0]));
	}
	elseif ($_POST['type'] == 'day') {
		$date = explode('.', $_POST['id']);
		$query = new wp_query(array('post_status'=>'publish', 'paged' => $_POST['page'], 'year' => $date[2], 'monthnum' => $date[1], 'day' => $date[0]));
	}

	


	$all = $query->post_count;

	if ($all != 0):
		$lost = $all%4;
		$bd = array('', 'bd-noc bd-nor', 'bd-nor','');
	?>

	

	<div class="b-section bd bd-1111 bd-0 bd-tab50 nobd <?php if ($all == $lost) echo $bd[$lost]?>">
		<div class="bd-1111__bd"></div>

		<?php while($query->have_posts()):

		$query->the_post();
		$count++;

		if ($count%4==1) {$class = 'pad-r'; $dateClass = '';}
		elseif ($count%4==2) {$class = 'tab-nopad tab-pad-l'; $dateClass = 'pad-l';}
		elseif ($count%4==3) {$class = 'tab-nopad tab-pad-r'; $dateClass = 'pad-l tab-nopad';}
		elseif ($count%4==0) {$class = 'pad-l'; $dateClass = 'pad-l';}
		?>
		<a class="b-section__one b-section__one_tab3 <?=$class?> js-height" data-height="section08" href="<?php the_permalink();?>">
			<span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall')?>" alt="" title="" width="270" height="178"></span>
			<span class="b-section__one__title ff-ss"><?php the_title();?></span>
			<?=get_subheading()?>
			<span class="b-section__one__date <?=$dateClass?> date ff-ss"><?=get_norm_date(false)?></span>
		</a>


		

		<?php if ($count%4==0 || $count == $all):?>
			<div class="br"></div>
		</div>
		<?php endif;?>
		<?php if ($count%4==0 && $all > $count):?> 

        <div class="b-section bd bd-1111 bd-0 bd-tab50 nobd <?php if ($count == $all-$lost) echo $bd[$lost]?> ">
            <div class="bd-1111__bd"></div>
        <?php endif;?>
	<?php
	endwhile;

	else:
		echo 'end';
	endif;
	exit();

}

add_action('wp_ajax_nopriv_author_ajax', 'author_ajax'); 
add_action('wp_ajax_author_ajax', 'author_ajax');






function search_results_ajax() {
	if ($_POST['type'] == 'search'):
		$query = new wp_query(array('post_status'=>'publish', 'paged' => $_POST['page'], 's' => $_POST['id'], 'post_type' => 'post' ));

		$all = $query->post_count;

		if ($all != 0):


			while($query->have_posts()):

				$query->the_post();

				?>

				<div class="b-list__section__items__one">
                            <?php if (has_post_thumbnail()):?>
                            <div class="b-list__section__items__one__image"><a href="<?php the_permalink();?>"><img src="<?=get_thumb_url('cryptusSmall')?>" alt="<?=get_thumb_caption()?>" title="" width="270" height="178"></a></div>
                            <div class="b-list__section__items__one__text">
                            <?php endif;?>
                                <a href="<?php the_permalink();?>"><?=highlight_results(get_the_title(),$_POST['id'])?></a>
                                <div class="b-list__section__items__one__descr ff-s">
                                     <?=extract_search_content(get_the_content(),$_POST['id'])?> 
                                </div>
                                <div class="b-list__section__items__one__date date"><?=get_norm_date(false)?></div>
                            <?php if (has_post_thumbnail()):?>
                            </div>
                             <?php endif;?>
                        </div>

				<?php

			endwhile;

		else:
			echo 'end';
		endif;
	endif;
	exit();


}

add_action('wp_ajax_nopriv_search_results_ajax', 'search_results_ajax'); 
add_action('wp_ajax_search_results_ajax', 'search_results_ajax');




function mainpage_ajax() {
	$exclude = explode(',',$_POST['exclude']);
	$page = intval($_POST['page']);
	$posts = get_posts(
		array(
			'posts_per_page' => 20,
			'paged' => $page,
			'post_type' => 'post',
			'post_status' => 'publish',
			'post__not_in' => $exclude
		)
	);

	foreach ($posts as $item):?>

	<div class="b-list__section__items__one">
		<div class="b-list__section__items__one__image">
			<a href="<?=get_permalink($item)?>" class="b-list__section__items__one__link">
				<img src="<?=get_thumb_url('cryptusSmall', $item)?>" alt="" title="" width="179" height="126">
			</a>
		</div>
		<div class="b-list__section__items__one__text">
			<a href="<?=get_permalink($item)?>" class="b-list__section__items__one__link">
				<div><?=$item->post_title?></div>
				<div class="b-list__section__items__one__descr ff-s">
					<?=get_subheading($item->ID)?>
				</div>
			</a>
			<div class="b-list__section__items__one__date date"><?php if (get_label($item->ID)):?><?=get_label($item->ID)?> <span>&middot;</span><?php endif;?> <?=get_norm_date(false,$item)?></div>
		</div>
	</div>
	<?php
	endforeach;
	exit();
}


add_action('wp_ajax_nopriv_mainpage_ajax', 'mainpage_ajax'); 
add_action('wp_ajax_mainpage_ajax', 'mainpage_ajax');

?>