<?php

add_image_size( 'author', 120, 120, true );
add_image_size( 'authorBig', 300, 300, true );

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );
 
function my_show_extra_profile_fields( $user ) { ?>
	
	<style>
		#authorImageAppend img {
			margin-bottom: 10px;
		}
	</style>
 
    <h3>Фотография автора</h3>
 
    <table class="form-table">
 
        <tr>
            <th><label for="authorImage">Фото:</label></th>
 
            <td>
            	<div id="authorImageAppend">
            		<?php if (get_the_author_meta( 'authorImage', $user->ID )):?>
            			<img src="<?=get_author_photo($user->ID)?>"/>
            			<?php $choose = 'none'; $remove = 'inline-block';?>
            		<?php else:?>
            			<?php $remove = 'none'; $choose = 'inline-block';?>
            		<?php endif;?>
            	</div>
            	<span class="button" id="authorImageAdd" style="display:<?=$choose?>">Выбрать изображение</span>
            	<span class="button" id="authorImageRemove" style="display:<?=$remove?>">Удалить изображение</span>
                <input type="hidden" name="authorImage" id="authorImage" value="<?=get_the_author_meta( 'authorImage', $user->ID )?>" /><br/><br/>
                <span class="description">Желательно, чтобы фотография была квадратной</span>
            
            </td>
        </tr>
 
    </table>

<?php }


add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );
 
function my_save_extra_profile_fields( $user_id ) {
 
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
 
    /* Copy and paste this line for additional fields. Make sure to change 'authorImage' to the field ID. */
    update_usermeta( absint( $user_id ), 'authorImage', wp_kses_post( $_POST['authorImage'] ) );
}

function author_admin_scripts($hook) {

    if ( $hook == 'user-edit.php' || $hook == 'profile.php' ) {
        
          wp_enqueue_media();
          wp_register_script('authorImage', get_template_directory_uri().'/functions/author-image.js', array('jquery','media-upload','thickbox'), false, true);
          wp_enqueue_script('authorImage');
        
    }  
    
}
add_action('admin_enqueue_scripts', 'author_admin_scripts');


function get_author_photo($id, $size = 'author') {
	$photo = get_user_meta($id, 'authorImage', true);
	$url = wp_get_attachment_image_src($photo, $size, true);
	return $url[0];
}

function author_column_style() {
    ?>

    <style>
        .column-coauthors {
            width:15%;
        }
    </style>

    <?php
}

add_action('admin_head', 'author_column_style');


?>