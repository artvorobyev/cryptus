$(document).ready(function() {

	$( ".js-sortable" ).sortable();
	$( ".js-sortable" ).disableSelection();
	

	$('.gallery__add').live('click', function() {
		var id = parseInt($(this).data('id'));

		var custom_uploader;

		if ( custom_uploader ) {
			custom_uploader.open();
			return;
		}

		custom_uploader = wp.media.frames.custom_uploader = wp.media({
			title: 'Выберите файл',
			button: {
				text: $( this ).data( 'uploader_button_text' )
			},
			multiple: true
		});

		custom_uploader.on('select', function() {
			var count = $('#gallery'+id+'__count').val();
			count = parseInt(count);
			attachments = custom_uploader.state().get('selection').toJSON();
			$.each(attachments, function(i, attachment) {
				count++;
				var image = '<img src="' + attachments[i].sizes.thumbnail.url + '" class="gallery__image"/>';
				var result = '<div class="gallery__point" id="' + count + '"><input type="hidden" class="gallery'+id+'__id" name="gallery'+id+'__id'+ count+ '" id="gallery'+id+'__id'+ count+ '" value="'+ attachments[i].id+ '" />'+ image+ '<span class="gallery__delete" data-id="'+ id + '" data-delete="'+ count+ '">×</span>';
				$('.gallery__container[data-id="'+id+'"]').append(result);
				$('#gallery'+id+'__count').val(count).trigger('change');
			});

			

		});

		custom_uploader.open();
	});


	$('.gallery__delete').live('click', function() {
		var id = ($(this).attr('data-delete'));
		var gallery = ($(this).attr('data-id'));
		var count = parseInt($('#gallery'+gallery+'__count').val());
		
		if (id < count) {
			$(this).closest('.gallery__container').find('.gallery__point').each(function(index, element) {
				var new_id = parseInt($(element).attr('id'));
				if (new_id > id) {
					var var_id = parseInt(new_id - 1);
					$(element).attr('id', var_id);
					$(element).find('.gallery__delete').first().attr('data-delete', var_id);
					$(element).find('input').each(function() {
						var el_class = $(this).attr('class');
						var el_name = el_class + var_id;
						$(this).attr('name', el_name);
						$(this).attr('id', el_name)
					});
				}
			});
		}

		$(this).closest('.gallery__point').remove();

		var new_count = parseInt(count) - 1;
		$('#gallery'+gallery+'__count').val(new_count).trigger('change');
	});

	$('.gallery__container').live( "sortstop", function(event, ui) {
		$(this).find('.gallery__point').each(function(index, element) {
			var id = index + 1
			$(this).attr('id', id);
			$(this).find('.gallery__delete').first().attr('data-delete', id);
			$(this).find('input, textarea').each(function() {
				var el_class = $(this).attr('class');
				var el_name = el_class + id;
				$(this).attr('name', el_name);
				$(this).attr('id', el_name)
			})
		})
	});



	//новая галерея

	$('.gallery__new').click(function() {
		var all = parseInt($('#gallery__all').val());
		var new_all = all+1;

		var append = '<div class="gallery__item" data-gallery="'+new_all+'"> <div class="gallery__heading" data-id="'+new_all+'">Галерея '+new_all+'</div> <div class="gallery__code" data-id="'+new_all+'">[fotorama id="'+new_all+'"]</div> <div class="gallery__remove" data-remove="'+new_all+'">удалить галерею</div> <div class="gallery__container js-sortable" data-id="'+new_all+'"></div> <a href="javascript:void(0)" class="gallery__add" data-id="'+new_all+'">Добавить изображение</a> <input type="hidden" name="gallery'+new_all+'__count" id="gallery'+new_all+'__count" value="0"/> </div>';

		$('#gallery__append').append(append);

		$('#gallery__all').val(new_all);

		$('.gallery__container[data-id="'+new_all+'"]').sortable().disableSelection();

	});


	//удаление галереи


	$('.gallery__remove').live('click', function() {
		var remove = parseInt($(this).data('remove'));
		var all = parseInt($('#gallery__all').val());

		alert(remove);

		

		if (remove < all) {
			$('.gallery__item').each(function(i, item) {
				var gallery = parseInt($(item).data('gallery') );


				if (gallery > remove) {
					
					var thisID = gallery;
					var gallery = gallery - 1;

					alert(thisID +',' + gallery);

					$('[data-id="'+thisID+'"]').each( function(index, element) {
						$(element).attr('data-id', gallery);

						if ($(element).hasClass('gallery__heading')) {
							$(element).text('Галерея' + gallery);
						}

						if ($(element).hasClass('gallery__code')) {
							$(element).text('[fotorama id="' + gallery + '"]');
						}
					});

					$('[data-remove="'+thisID+'"]').attr('data-remove', gallery);

					$('#gallery'+thisID+'__count').attr('name', 'gallery'+gallery+'__count');
					$('#gallery'+thisID+'__count').attr('id', 'gallery'+gallery+'__count');

					$('.gallery'+thisID+'__id').each(function(index, element){
						var newID = $(element).parent().attr('id');
						var newClass = 'gallery'+gallery+'__id';
						var newName = newClass+newID;
						$(element).attr('class', newClass);
						$(element).attr('name', newName);
						$(element).attr('id', newName);

					});

					$('.gallery__delete[data-id="'+thisID+'"]').each(function(index, element) {
						$(element).data('id', gallery);
					});
				}
				


			});
		}

		$(this).closest('.gallery__item').remove();
		var new_all = all-1;
		$('#gallery__all').val(new_all);

		

	});
});