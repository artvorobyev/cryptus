<?php




function add_banner_menu_item()
{
	add_submenu_page('edit.php?post_type=banner', "Расстановка баннеров", "Расстановка баннеров", "edit_others_posts", "banner-panel", "banner_theme_settings_page");
}

add_action("admin_menu", "add_banner_menu_item");


function select_banners($value = null) {
    $posts = get_posts(array('post_type' => 'banner', 'posts_per_page' => -1));
    foreach ($posts as $i=>$item):
        if ($item->ID == $value) $sel[$i] = ' selected ';
        ?>
            <option value="<?=$item->ID?>" <?=$sel[$i]?>><?=$item->post_title?></option>
        <?php
    endforeach;
}



function banner_theme_settings_page()
{

        if (isset($_GET['settings-updated'])) {
            echo '<div id="message" class="updated fade"><p><strong>Изменения сохранены</strong></p></div>';
        }
    ?>
        <div class="wrap">
        
        <h1>Расстановка баннеров</h1>
        <form method="post" action="options.php">
            <?php
                settings_fields("banner_section");
                do_settings_sections("banner-options");      
                submit_button(); 
            ?>          
        </form>
        </div>
    <?php
}


function display_index_1h()
{
    ?>
        <select name="index_1h" id="index_1h" style="width:50%;">
            <?=select_banners(get_option('index_1h'))?>
        </select>
    <?php
}

function display_index_1v()
{
    ?>
        <select name="index_1v" id="index_1v" style="width:50%;">
            <?=select_banners(get_option('index_1v'))?>
        </select>
    <?php
}

function display_index_2h()
{
    ?>
        <select name="index_2h" id="index_2h" style="width:50%;">
            <?=select_banners(get_option('index_2h'))?>
        </select>
    <?php
}

function display_index_2v()
{
    ?>
        <select name="index_2v" id="index_2v" style="width:50%;">
            <?=select_banners(get_option('index_2v'))?>
        </select>
    <?php
}

function display_index_3v()
{
    ?>
        <select name="index_3v" id="index_3v" style="width:50%;">
            <?=select_banners(get_option('index_3v'))?>
        </select>
    <?php
}


function display_index_3h()
{
    ?>
        <select name="index_3h" id="index_3h" style="width:50%;">
            <?=select_banners(get_option('index_3h'))?>
        </select>
    <?php
}

function display_index_4v()
{
    ?>
        <select name="index_4v" id="index_4v" style="width:50%;">
            <?=select_banners(get_option('index_4v'))?>
        </select>
    <?php
}

function display_index_4h()
{
    ?>
        <select name="index_4h" id="index_4h" style="width:50%;">
            <?=select_banners(get_option('index_4h'))?>
        </select>
    <?php
}








//==Категории===================


function display_category_1h()
{
    ?>
        <select name="category_1h" id="category_1h" style="width:50%;">
            <?=select_banners(get_option('category_1h'))?>
        </select>
    <?php
}

function display_category_1v()
{
    ?>
        <select name="category_1v" id="category_1v" style="width:50%;">
            <?=select_banners(get_option('category_1v'))?>
        </select>
    <?php
}

function display_category_2h()
{
    ?>
        <select name="category_2h" id="category_2h" style="width:50%;">
            <?=select_banners(get_option('category_2h'))?>
        </select>
    <?php
}

function display_category_2v()
{
    ?>
        <select name="category_2v" id="category_2v" style="width:50%;">
            <?=select_banners(get_option('category_2v'))?>
        </select>
    <?php
}








//==========     Архив событий         ===================


function display_event_archive_1h()
{
    ?>
        <select name="event_archive_1h" id="event_archive_1h" style="width:50%;">
            <?=select_banners(get_option('event_archive_1h'))?>
        </select>
    <?php
}

function display_event_archive_1v()
{
    ?>
        <select name="event_archive_1v" id="event_archive_1v" style="width:50%;">
            <?=select_banners(get_option('event_archive_1v'))?>
        </select>
    <?php
}

function display_event_archive_2h()
{
    ?>
        <select name="event_archive_2h" id="event_archive_2h" style="width:50%;">
            <?=select_banners(get_option('event_archive_2h'))?>
        </select>
    <?php
}










//==========      Cобытие         ===================


function display_event_single_1h()
{
    ?>
        <select name="event_single_1h" id="event_single_1h" style="width:50%;">
            <?=select_banners(get_option('event_single_1h'))?>
        </select>
    <?php
}

function display_event_single_1v()
{
    ?>
        <select name="event_single_1v" id="event_single_1v" style="width:50%;">
            <?=select_banners(get_option('event_single_1v'))?>
        </select>
    <?php
}

function display_event_single_2h()
{
    ?>
        <select name="event_single_2h" id="event_single_2h" style="width:50%;">
            <?=select_banners(get_option('event_single_2h'))?>
        </select>
    <?php
}











//==========      Статья с баннером         ===================


function display_article_1h()
{
    ?>
        <select name="article_1h" id="article_1h" style="width:50%;">
            <?=select_banners(get_option('article_1h'))?>
        </select>
    <?php
}

function display_article_1v()
{
    ?>
        <select name="article_1v" id="article_1v" style="width:50%;">
            <?=select_banners(get_option('article_1v'))?>
        </select>
    <?php
}

function display_article_2h()
{
    ?>
        <select name="article_2h" id="article_2h" style="width:50%;">
            <?=select_banners(get_option('article_2h'))?>
        </select>
    <?php
}









//==========      Поиск         ===================


function display_search_1h()
{
    ?>
        <select name="search_1h" id="search_1h" style="width:50%;">
            <?=select_banners(get_option('search_1h'))?>
        </select>
    <?php
}

function display_search_1v()
{
    ?>
        <select name="search_1v" id="search_1v" style="width:50%;">
            <?=select_banners(get_option('search_1v'))?>
        </select>
    <?php
}








//==========      Остальные страницы (автор, тег)         ===================


function display_other_1h()
{
    ?>
        <select name="other_1h" id="other_1h" style="width:50%;">
            <?=select_banners(get_option('other_1h'))?>
        </select>
    <?php
}

function display_other_2h()
{
    ?>
        <select name="other_2h" id="other_2h" style="width:50%;">
            <?=select_banners(get_option('other_2h'))?>
        </select>
    <?php
}



function display_banner_panel_fields()
{
    add_settings_section("index_banners", "Главная страница", null, "banner-options");
    
    add_settings_field("index_1h", "Первый верхний горизонтальный (970×250)", "display_index_1h", "banner-options", "index_banners");
    register_setting("banner_section", "index_1h");


    add_settings_field("index_1v", "Первый вертикальный (300×600)", "display_index_1v", "banner-options", "index_banners");
    register_setting("banner_section", "index_1v");


    add_settings_field("index_2h", "Второй горизонтальный (970×250)", "display_index_2h", "banner-options", "index_banners");
    register_setting("banner_section", "index_2h");

    add_settings_field("index_2v", "Второй вертикальный (300×600)", "display_index_2v", "banner-options", "index_banners");
    register_setting("banner_section", "index_2v");

    add_settings_field("index_3v", "Третий вертикальный (300×600)", "display_index_3v", "banner-options", "index_banners");
    register_setting("banner_section", "index_3v");

    add_settings_field("index_3h", "Третий горизонтальный (970×120)", "display_index_3h", "banner-options", "index_banners");
    register_setting("banner_section", "index_3h");

    add_settings_field("index_4v", "Четвертый вертикальный (300×600)", "display_index_4v", "banner-options", "index_banners");
    register_setting("banner_section", "index_4v");

    add_settings_field("index_4h", "Четвертый нижний горизонтальный (970×250)", "display_index_4h", "banner-options", "index_banners");
    register_setting("banner_section", "index_4h");


    //==========Категории================


    add_settings_section("category_banners", "Страница любого раздела", null, "banner-options");
    
    add_settings_field("category_1h", "Первый верхний горизонтальный (970×250)", "display_category_1h", "banner-options", "category_banners");
    register_setting("banner_section", "category_1h");


    add_settings_field("category_1v", "Первый вертикальный (300×600)", "display_category_1v", "banner-options", "category_banners");
    register_setting("banner_section", "category_1v");


    add_settings_field("category_2h", "Второй горизонтальный (970×250)", "display_category_2h", "banner-options", "category_banners");
    register_setting("banner_section", "category_2h");

    add_settings_field("category_2v", "Второй вертикальный (300×600)", "display_category_2v", "banner-options", "category_banners");
    register_setting("banner_section", "category_2v");



    //==========   Архив событий  ================


    add_settings_section("event_archive_banners", "Список событий", null, "banner-options");
    
    add_settings_field("event_archive_1h", "Первый верхний горизонтальный (970×250)", "display_event_archive_1h", "banner-options", "event_archive_banners");
    register_setting("banner_section", "event_archive_1h");


    add_settings_field("event_archive_1v", "Первый вертикальный (300×600)", "display_event_archive_1v", "banner-options", "event_archive_banners");
    register_setting("banner_section", "event_archive_1v");


    add_settings_field("event_archive_2h", "Второй горизонтальный (970×250)", "display_event_archive_2h", "banner-options", "event_archive_banners");
    register_setting("banner_section", "event_archive_2h");








    //==========   Событие  ================


    add_settings_section("event_single_banners", "Страница любого события", null, "banner-options");
    
    add_settings_field("event_single_1h", "Первый верхний горизонтальный (970×250)", "display_event_single_1h", "banner-options", "event_single_banners");
    register_setting("banner_section", "event_single_1h");


    add_settings_field("event_single_1v", "Первый вертикальный (300×600)", "display_event_single_1v", "banner-options", "event_single_banners");
    register_setting("banner_section", "event_single_1v");


    add_settings_field("event_single_2h", "Второй горизонтальный (970×250)", "display_event_single_2h", "banner-options", "event_single_banners");
    register_setting("banner_section", "event_single_2h");









    //==========   Статья с баннером  ================


    add_settings_section("article_banners", "Страница любой статьи с баннером", null, "banner-options");
    
    add_settings_field("article_1h", "Первый верхний горизонтальный (970×250)", "display_article_1h", "banner-options", "article_banners");
    register_setting("banner_section", "article_1h");


    add_settings_field("article_1v", "Первый вертикальный (300×600)", "display_article_1v", "banner-options", "article_banners");
    register_setting("banner_section", "article_1v");


    add_settings_field("article_2h", "Второй горизонтальный (970×250)", "display_article_2h", "banner-options", "article_banners");
    register_setting("banner_section", "article_2h");









     //==========   Результаты поиска  ================


    add_settings_section("search_banners", "Результаты поиска", null, "banner-options");
    
    add_settings_field("search_1h", "Первый верхний горизонтальный (970×250)", "display_search_1h", "banner-options", "search_banners");
    register_setting("banner_section", "search_1h");


    add_settings_field("search_1v", "Первый вертикальный (300×600)", "display_search_1v", "banner-options", "search_banners");
    register_setting("banner_section", "search_1v");









     //==========   Остальные страницы (автор, тег)  ================


    add_settings_section("other_banners", "Остальные страницы (автор, тег)", null, "banner-options");
    
    add_settings_field("other_1h", "Первый верхний горизонтальный (970×250)", "display_other_1h", "banner-options", "other_banners");
    register_setting("banner_section", "other_1h");

    add_settings_field("other_2h", "Второй горизонтальный (970×250)", "display_other_2h", "banner-options", "other_banners");
    register_setting("banner_section", "other_2h");




}


add_action("admin_init", "display_banner_panel_fields");





?>