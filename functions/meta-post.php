<?php

function articles_custom_boxes() {
	$screens = array( 'post');
	global $post;
	foreach ( $screens as $screen ) {

		add_meta_box( 'image_remember', 'Изображение', 'image_remember_meta_box_callback', $screen, 'extra', 'high' );//выше редактора

		add_meta_box( 'subheading', 'Подзаголовок', 'subheading_meta_box_callback', $screen, 'extra', 'high' );//выше редактора
		add_meta_box( 'lead', 'Лид', 'lead_meta_box_callback', $screen, 'extra', 'high' );//выше редактора
		add_meta_box( 'main', 'Главное', 'main_meta_box_callback', $screen, 'side', 'low' );//справа
		add_meta_box( 'partner', 'Рекламные материалы', 'partner_meta_box_callback', $screen );
		add_meta_box( 'day', 'Избранные дни', 'day_meta_box_callback', $screen );
		add_meta_box( 'lead_socials', 'Лид для соцсетей', 'lead_socials_meta_box_callback', $screen );

		

		//справа
		

		

		
		
	}
}

add_action('add_meta_boxes', 'articles_custom_boxes');








function subheading_meta_box_callback() {
		global $post;
	?>
		<input type="text" style="width:90%;" name="subheading" value="<?=get_post_meta($post->ID, 'subheading', true)?>"/>
		<div class="js-controller">90</div>
	<?php
}

function lead_meta_box_callback() {
		global $post;
	?>
		<textarea style="width:100%; height:100px;" name="lead"><?=get_post_meta($post->ID, 'lead', true)?></textarea>
	<?php
}



function lead_socials_meta_box_callback() {
		global $post;
	?>
		<textarea style="width:100%; height:100px;" name="lead_socials"><?=get_post_meta($post->ID, 'lead_socials', true)?></textarea><br/>
		<div class="likely" data-url="<?php the_permalink();?>" data-title="<?php the_title();?>">
		  <div class="twitter">Твитнуть</div>
		  <div class="facebook">Поделиться</div>
		  <div class="vkontakte">Поделиться</div>
		  <div class="odnoklassniki">Класснуть</div>
		</div>
	<?php
}


function image_remember_meta_box_callback() {
		global $post;

		if (has_post_thumbnail($post)):
			$image_size = getimagesize(get_thumb_url('cryptusHuge', $post));

			if ($image_size[0] != 1200 || $image_size[1] != 800):

	?>
				<div class="notice notice-error">
					<p><strong>Предупреждение!<br/>Размеры изображения не соответствуют стандартам!</strong></p>
				</div>
	<?php
			endif;
		endif;
	?>

	<input type="text" style="width:100%;" name="subheading" value="<?=get_post_meta($post->ID, 'CryptusBigUrl', true)?>"/>
		

	<?php
}




function create_new_image_by_proportion( $post_id ) {
	global $post;
	$post = get_post($post_id);

	$previous = intval(get_post_meta($post_id,'previous_thumbnail',true));
	$thumb_id = intval($_POST['_thumbnail_id']);

	if (has_post_thumbnail($post) && $post->post_type == 'post' && $previous != $thumb_id) {

		update_post_meta($post_id,'previous_thumbnail',$thumb_id);

    	$thumb_url = wp_get_attachment_image_src($thumb_id, 'cryptusHuge', true);

		$image_size = getimagesize($thumb_url[0]);

		if ($image_size[0] != 1200 || $image_size[1] != 800) {

			$file = get_attached_file($thumb_id);

			$width = $image_size[0];
	        $height = $image_size[1];

	        $new_width = $width;
	        $new_height = $new_width*(2/3);

	        if ($new_height > $height ) {
	          $new_height = $height;
	          $new_width = 1.5 * $new_height;
	        }


	        $image_p = imagecreatetruecolor($new_width, $new_height);

	        if ($image_size['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file);
	        elseif ($image_size['mime'] == 'image/png') $image = imagecreatefrompng($file);


	        if ($image != false) {

	        	$dir = wp_upload_dir();
		        $fileName = $dir['path'] . '/CryptusBig_'. $post_id . '.jpg';

		        $image_p = imagecrop($image, ['x' => 0, 'y' => 0, 'width' => $new_width, 'height' => $new_height]);

		        // вывод
		        imagejpeg($image_p, $fileName, 100);



	        
	        	$filetype = wp_check_filetype( basename( $fileName ), null );
	        	$attachment = array(
					'guid'           => $dir['url'] . '/' . basename( $fileName ), 
					'post_mime_type' => $filetype['type'],
					'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $fileName ) ),
					'post_content'   => '',
					'post_status'    => 'inherit'
				);

				$attach_id = wp_insert_attachment( $attachment, $fileName, intval($post_id) );

				// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				 
				// Generate the metadata for the attachment, and update the database record.
				$attach_data = wp_generate_attachment_metadata( $attach_id, $fileName );
				wp_update_attachment_metadata( $attach_id, $attach_data );

				$url = wp_get_attachment_image_src($attach_id, false, true);

				update_post_meta($post_id, 'CryptusBigUrl', $url[0]);
				update_post_meta($post_id, 'CryptusBigID', $attach_id);
			}
		}
	}
}

add_action( 'save_post', 'create_new_image_by_proportion' );











function articles_text_save_postdata( $post_id ) {
	

	if (isset($_REQUEST['subheading'])):
		update_post_meta( $post_id, 'subheading', htmlspecialchars($_POST['subheading']) );
	endif;

	if (isset($_REQUEST['lead'])):
		update_post_meta( $post_id, 'lead', htmlspecialchars($_POST['lead']) );
	endif;

	if (isset($_REQUEST['partner_url'])):
		update_post_meta( $post_id, 'partner_url', $_POST['partner_url'] );
	endif;

	if (isset($_REQUEST['partner_logo'])):
		update_post_meta( $post_id, 'partner_logo', $_POST['partner_logo'] );
	endif;

	if (isset($_REQUEST['lead_socials'])):
		update_post_meta( $post_id, 'lead_socials', htmlspecialchars($_POST['lead_socials']) );
	endif;

	$array = array('day_date','day_month');

	foreach ($array as $item) {
		if (isset($_REQUEST[$item])):
			update_post_meta( $post_id, $item, htmlspecialchars($_POST[$item]) );
		endif;
	}
	

		
}

add_action( 'save_post', 'articles_text_save_postdata' );





function partner_meta_box_callback() {
	global $post;
	$value1 = get_post_meta($post->ID, 'is_banner', true);
	if ($value1 == '1') $checked1 = 'checked';
	?>
		<br/>
		<input type="checkbox" name="is_banner" value="1" <?=$checked1?> /> Не показывать баннер<br/><br/>
	<?php
	$value = get_post_meta($post->ID, 'is_partner', true);
	if ($value == '1') $checked = 'checked';
	?>
		<input type="checkbox" name="is_partner" id="is_partner" value="1" <?=$checked?> /> Сделать этот материал партнерским

		<div id="partner_elements" <?php if($value != '1'):?> style="display: none"<?php endif;?> >
			<br/>
			<strong>Логотип партнера:</strong>
			<div id="partner_here"></div>
			<?php
			$id = get_post_meta($post->ID, 'partner_logo', true);

		    if ($id):

		    	$url = wp_get_attachment_image_src($id, '', true)[0];

			    ?>

			    <img src="<?=$url?>" id="partner_image"/>

			    <input type="hidden" name="partner_logo" id="partner_logo" value="<?=$id?>" />

			    <div class="logo__buttons">
			    	<a href="javascript:void(0)" class="logo__choose" style="display: none;">Выбрать изображение</a>
			    	<a href="javascript:void(0)" class="logo__delete">Удалить изображение</a>
			    </div>
				
			    <?php

		   	else:

			    ?>

			    <input type="hidden" name="partner_logo" id="partner_logo" value=""/>

			    <div class="logo__buttons">
			    	<a href="javascript:void(0)" class="logo__choose">Выбрать изображение</a>
			    	<a href="javascript:void(0)" class="logo__delete" style="display: none;">Удалить изображение</a>
			    </div>
				
			    <?php

		    endif;
		    ?>
		    <br/>
			<div><strong>Ссылка на партнера:</strong></div>
			<input type="url" style="width:100%;" name="partner_url" value="<?=get_post_meta($post->ID, 'partner_url', true)?>"/>
	
		</div>
	<?php
}

function partner_save_postdata( $post_id ) {

	if (isset($_REQUEST['is_partner'])):

	$value = $_POST['is_partner'];

	if ($value != '1') $value = 0;

	update_post_meta( $post_id, 'is_partner', $value);

	endif;


		
}

add_action( 'save_post', 'partner_save_postdata' );






function banner_save_postdata( $post_id ) {
	
	if (isset($_REQUEST['is_banner'])):

	$value = $_POST['is_banner'];

	if ($value != '1') $value = 0;

	update_post_meta( $post_id, 'is_banner', $value);

	endif;


		
}

add_action( 'save_post', 'banner_save_postdata' );




function show_thumb_save_postdata( $post_id ) {
	
	if (isset($_REQUEST['is_show_thumb'])):

	$value = $_POST['is_show_thumb'];

	if ($value != '1') $value = 0;

	update_post_meta( $post_id, 'is_show_thumb', $value);

	endif;


		
}

add_action( 'save_post', 'show_thumb_save_postdata' );





function main_meta_box_callback() {
	global $post;
	$value = get_post_meta($post->ID, 'is_main', true);
	if ($value == '1') $checked = 'checked';
	?>
		<br/>
		<input type="checkbox" name="is_main" value="1" <?=$checked?> /> Показывать в блоке «Главное»<br/><br/>

	<?php
	$value1 = get_post_meta($post->ID, 'main_news', true);
	if ($value1 == '1') $checked1 = 'checked';
	?>
		<input type="checkbox" name="main_news" value="1" <?=$checked1?> /> Показывать в блоке «Главные новости»<br/><br/>
	<?php
	$value2 = get_post_meta($post->ID, 'is_show_thumb', true);
	if ($value2 == '1') $checked2 = 'checked';
	?>
		<input type="checkbox" name="is_show_thumb" value="1" <?=$checked2?> /> Скрыть изображение на странице статьи<br/><br/>

	<?php
	$value3 = get_post_meta($post->ID, 'show_author', true);
	if ($value3 == '1') $checked3 = 'checked';
	?>
		<input type="checkbox" name="show_author" value="1" <?=$checked3?> /> Показывать имя автора
	<?php
}

function main_save_postdata( $post_id ) {
	
	if (isset($_REQUEST['is_main'])):

	$value = $_POST['is_main'];

	if ($value != '1') $value = 0;

	update_post_meta( $post_id, 'is_main', $value);

	endif;


		
}

add_action( 'save_post', 'main_save_postdata' );


function main_news_save_postdata( $post_id ) {
	
	if (isset($_REQUEST['main_news'])):


	$value = $_POST['main_news'];

	if ($value != '1') $value = 0;

	update_post_meta( $post_id, 'main_news', $value);

	endif;


		
}

add_action( 'save_post', 'main_news_save_postdata' );


function day_meta_box_callback() {
	global $post;
	$value = get_post_meta($post->ID, 'is_day', true);
	if ($value == '1') $checked = 'checked';
	?>
		<input type="checkbox" name="is_day" id="is_day" value="1" <?=$checked?> /> Показывать в блоке «Избранные дни» <br/><br/>
		<table style="width:300px" id="day_table">
			<tr>
				<td style="width:100px; padding-right:20px;">
					<b><label>Число:</label></b><br/>
					<input type="text" name="day_date" value="<?=get_post_meta($post->ID,'day_date',true)?>"/>
				</td>
				<td style="width:100px; padding-right:20px;">
					<b><label>Месяц:</label></b><br/>
					<input type="text" name="day_month" value="<?=get_post_meta($post->ID,'day_month',true)?>"/>
					
				</td>
				
			</tr>
		</table>

	

	<?php
}

function day_save_postdata( $post_id ) {
	
	if (isset($_REQUEST['is_day'])):


	$value = $_POST['is_day'];

	if ($value != '1') $value = 0;

	update_post_meta( $post_id, 'is_day', $value);

	endif;


		
}

add_action( 'save_post', 'day_save_postdata' );






function show_author_save_postdata( $post_id ) {
	
	if (isset($_REQUEST['show_author'])):

	$value = $_POST['show_author'];

	if ($value != '1') $value = 0;

	update_post_meta( $post_id, 'show_author', $value);

	endif;

		
}

add_action( 'save_post', 'show_author_save_postdata' );
















function foo_move_deck() {
        # Get the globals:
        global $post, $wp_meta_boxes;

        # Output the "advanced" meta boxes:
        do_meta_boxes( get_current_screen(), 'extra', $post );

        # Remove the initial "advanced" meta boxes:
        unset($wp_meta_boxes['post']['extra']);
    }

add_action('edit_form_after_title', 'foo_move_deck');

function articles_meta_styles() {
	global $post;
	if ($post->post_type == 'post'):
	?>
		<style>
			#extra-sortables {
			padding-top: 20px;
			}
			#insert-media-button {
				display: none;
			}
			#partner_image {
				max-width: 100px;
				margin:10px 0;
			}

			.js-controller {
				width: 10%;
				position: absolute;
				right:0;
				top:5px;
				text-align: center;
				font-size:20px;
			}

			.js-controller.red {
				color:red;
			}
			
			#title {
			    width: 89% !important;
			}

			#titlewrap .js-controller {
				top:10px;
			}

			#image_remember {
			    display: none;
			}

			#is_day ~ #day_table {
				display: none;
			}

			#is_day:checked ~ #day_table {
				display: table;
			}
		</style>
		<link rel="stylesheet" href="<?=get_template_directory_uri()?>/functions/likely.css"/>
	<?php
	endif;
}

add_action('admin_head', 'articles_meta_styles');



function admin_js() {
	global $post;
	if ($post->post_type == 'post'):
  ?>
    <script src="<?=get_template_directory_uri()?>/functions/admin.js?v=2"></script>
    <script src="<?=get_template_directory_uri()?>/functions/likely.js"></script>
  <?php
  endif;
}

add_action('admin_footer', 'admin_js', 1000);

?>