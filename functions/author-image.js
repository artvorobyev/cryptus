jQuery(document).ready(function($) {
	$('#authorImageAdd').live('click', function() {

		var custom_uploader;

		if ( custom_uploader ) {
			custom_uploader.open();
			return;
		}

		custom_uploader = wp.media.frames.custom_uploader = wp.media({
			title: 'Выберите файл',
			button: {
				text: $( this ).data( 'uploader_button_text' )
			},
			multiple: false
		});

		custom_uploader.on('select', function() {
			attachments = custom_uploader.state().get('selection').toJSON();
			$.each(attachments, function(i, attachment) {
				var image = '<img src="' + attachments[i].sizes.thumbnail.url + '"/>';
				$('#authorImageAppend').append(image);
				$('#authorImageAdd').hide();
				$('#authorImageRemove').show();
				$('#authorImage').val(attachments[i].id);
			});

			

		});

		custom_uploader.open();
	});

	$('#authorImageRemove').live('click', function() {
		$(this).hide();
		$('#authorImageAdd').show();
		$('#authorImageAppend img').remove();
		$('#authorImage').val('');

	});
});