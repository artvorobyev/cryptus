<?php


add_action('init', 'banner_custom_post_types');
function banner_custom_post_types() {

	global $wp_rewrite;
	
	register_post_type('banner', array(
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' =>  true,
		'supports' => array('title'),
		'has_archive' => true,
		'labels' => 
		    array(
		        'name' => 'Баннеры',
		        'singular_name' => 'Баннер',
		        'add_new' => 'Добавить баннер',
		        'add_new_item' => 'Добавить новый баннер',
		        'edit_item' => 'Редактировать код баннера',
		        'new_item' => 'Новый баннер',
		        'all_items' => 'Все баннеры',
		        'view_item' => 'Просмотр баннера',
		        'search_items' => 'Искать баннер',
		        'not_found' =>  'Не найдено баннеров',
		        'not_found_in_trash' => 'Не найдено баннеров в корзине', 
		        'parent_item_colon' => ''
	        ),
		'menu_icon' => 'dashicons-align-right'
	));
}

function banner_custom_boxes() {
	$screens = array( 'banner');
	foreach ( $screens as $screen ){
		add_meta_box( 'banner_code', 'Код баннера', 'banner_code_meta_box_callback', $screen );
	}
		
		
}

add_action('add_meta_boxes', 'banner_custom_boxes');










function banner_code_meta_box_callback() {
		global $post;
	?>
		<textarea style="width:100%; height:100px;" name="banner_code"><?=htmlspecialchars_decode(get_post_meta($post->ID, 'banner_code', true))?></textarea>
	<?php
}

function banner_text_save_postdata( $post_id ) {
	global $post;

	if ($post->post_type == 'banner') {
		// проверяем, если это автосохранение ничего не делаем с данными нашей формы.
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
			return $post_id;

		update_post_meta( $post_id, 'banner_code', htmlspecialchars($_POST['banner_code']) );
	}
	
}

add_action( 'save_post', 'banner_text_save_postdata' );

function get_banner_by_slug($page, $slug) {
	$option = get_option($page.'_'.$slug);
	$post = get_post($option);
	return htmlspecialchars_decode(get_post_meta($post->ID, 'banner_code', true));

}


function hide_banner_slug() {
	global $post;

	if ($post->post_type == 'banner'):
	?>
		<style>
			#edit-slug-box {
				display: none;
			}
		</style>
	<?php
	endif;
}

add_action('admin_head', 'hide_banner_slug');


?>