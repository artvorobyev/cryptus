<?php

function filter_date($date) {
  $pub_date = str_replace(
    array("%01", "%02", "%03", "%04", "%05", "%06", "%07", "%08", "%09", "%10", "%11", "%12"),
    array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября",   "ноября", "декабря"), $date
    );

  return $pub_date;
}


function get_norm_date($year = true, $post = false) {
  if (!post) global $post;
  if ($year) $format = 'j %m Y';
  else $format = 'j %m';

  return filter_date(get_the_date($format, $post->ID));

}


function get_file_ver($filename) {
 $file = WP_CONTENT_DIR . "/themes/" . get_template() . "/" . $filename;
 return (file_exists($file) ? date('ymdHis', filemtime($file)) : '0');
}

add_theme_support( 'post-thumbnails' );
add_image_size('cryptusHuge', 1200,800,true);
add_image_size('cryptusBig', 870,580,true);
add_image_size('cryptusMedium', 580,387,true);
add_image_size('cryptusSmall', 270,180,true);



function get_thumb_url($size = false, $post = false) {
  if (!$post) global $post;
  if (has_post_thumbnail($post)) {
    $thumb_id = get_post_thumbnail_id($post);
    $thumb_url = wp_get_attachment_image_src($thumb_id, $size, true);

    if ($size == 'cryptusBig') {
      $image_size = getimagesize($thumb_url[0]);

      if ($image_size[0] != 870 || $image_size[1] != 580 ) {

        if (get_post_meta($post->ID,'CryptusBigUrl',true)) return get_post_meta($post->ID,'CryptusBigUrl',true);

        else return $thumb_url[0];

        
      }

      else return $thumb_url[0];
    }

    elseif ($size == 'cryptusHuge') {
      $image_size = getimagesize($thumb_url[0]);

      if ( $image_size[0] != 1200 || $image_size[1] != 800 ) {

         if (get_post_meta($post->ID,'CryptusBigUrl',true)) return get_post_meta($post->ID,'CryptusBigUrl',true);

         else return $thumb_url[0];
        
      }

      else return $thumb_url[0];
    }

    elseif ($size == 'cryptusMedium') {
      $image_size = getimagesize($thumb_url[0]);

      if ( $image_size[0] != 580 || $image_size[1] != 387 ) {

         if (get_post_meta($post->ID,'CryptusBigUrl',true)) return get_post_meta($post->ID,'CryptusBigUrl',true);

         else return $thumb_url[0];
        
      }

      else return $thumb_url[0];
    }


    elseif ($size == 'cryptusSmall') {
      $image_size = getimagesize($thumb_url[0]);

      if ( $image_size[0] != 270 || $image_size[1] != 180 ) {

         if (get_post_meta($post->ID,'CryptusBigUrl',true)) return get_post_meta($post->ID,'CryptusBigUrl',true);

         else return $thumb_url[0];
        
      }

      else return $thumb_url[0];
    }

    else return $thumb_url[0];
  }

  else return;
}

function get_photo_caption($id = false) {
  $meta = wp_prepare_attachment_for_js($id);
  $caption = htmlspecialchars($meta['caption']);
  return $caption;
}

function get_thumb_caption($post = false) {
  if (!$post) global $post;
  if (has_post_thumbnail($post)) {
    $thumb_id = get_post_thumbnail_id($post);
    return get_photo_caption($thumb_id);
  }

  else return;
}

function get_norm_tags() {
  global $post;
  $posttags = get_the_tags();
  if ($posttags) {
    foreach($posttags as $tag) {
      echo '<a href="' . get_tag_link($tag->term_id) . '">#' . $tag->name . '</a>';
    }
  }
}

function get_norm_tags_no_print() {
  global $post;
  $posttags = get_the_tags();
  if ($posttags) {
    foreach($posttags as $tag) {
      return '<a href="' . get_tag_link($tag->term_id) . '">#' . $tag->name . '</a>';
    }
  }
}


function get_norm_cats() {
  global $post;
  $cats = get_the_category($post->ID);
  if ($cats) {
    foreach($cats as $key=>$cat) {
      $children = get_terms( $cat->taxonomy, array(
        'parent'    => $cat->term_id,
        'hide_empty' => false
        ) );
        // print_r($children); // uncomment to examine for debugging
      if(!$children) 
        
        echo '<a href="' . get_category_link($cat->term_id) . '">' . $cat->name . '</a>';

        if (count($cats)>1 && $key != count($cats)-1) echo '<span>&middot;</span>';
        
    }
  }
}

function get_norm_cats_no_links() {
  global $post;
  $cats = get_the_category($post->ID);
  if ($cats) {
    foreach($cats as $key=>$cat) {
      $children = get_terms( $cat->taxonomy, array(
        'parent'    => $cat->term_id,
        'hide_empty' => false
        ) );
        // print_r($children); // uncomment to examine for debugging
      if(!$children) 
        
        echo $cat->name;

        if (count($cats)>1 && $key != count($cats)-1) echo ', ';
        
    }
  }
}

function get_norm_cats_no_print() {
  global $post;
  $cats = get_the_category($post->ID);
  if ($cats) {
    foreach($cats as $key=>$cat) {
      $children = get_terms( $cat->taxonomy, array(
        'parent'    => $cat->term_id,
        'hide_empty' => false
        ) );
        // print_r($children); // uncomment to examine for debugging
      if(!$children) 
        
        return '<a href="' . get_category_link($cat->term_id) . '">' . $cat->name . '</a>';

        if (count($cats)>1 && $key != count($cats)-1) return '<span>&middot;</span>';
        
    }
  }
}


function get_subheading($post_id = false) {
  if (!$post_id) {
    global $post;
    $post_id = $post->ID;
  }
  return get_post_meta($post_id, 'subheading', true);
}






add_action( 'pre_get_posts',  'set_posts_per_page'  );
function set_posts_per_page( $query ) {

  global $wp_the_query;

  if ( ( ! is_admin() ) && ( $query === $wp_the_query ) && ( $query->is_category() ) && (! $query->is_category('events')) ) {
    $query->set( 'posts_per_page', 76 );
  }

  return $query;
}


function load_template_part($template_name, $part_name=null) {
    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}


function prepare_exclude($array) {
  foreach ($array as $posts) {
    foreach ($posts as $item) {
      $output[] = $item->ID;
    }
  }
    
  return $output;
  
}





function get_label($post_id) {
  $cats = get_the_category($post_id);
  $echo = 0;
  if ($cats) {
    foreach($cats as $key=>$cat) {
      $children = get_terms( $cat->taxonomy, array(
        'parent'    => $cat->term_id,
        'hide_empty' => false
        ) );
        // print_r($children); // uncomment to examine for debugging
      if(!$children && !$echo) 
        
        return '<a href="'. get_category_link($cat->term_id) .'">' . $cat->name . '</a>';
        $echo++;

        
    }
  }
} 



?>