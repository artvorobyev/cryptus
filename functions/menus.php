<?php



add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);
function my_css_attributes_filter($var) {
  return is_array($var) ? array_intersect($var, array('menu-item-has-children')) : '';
}



add_action('after_setup_theme', function(){
  register_nav_menus( array(
    'header_menu' => 'Верхнее меню',
    'left_menu' => 'Раскрывающееся меню',
    'footer_menu' => 'Меню внизу страницы'
    ) );
});


function get_header_nav() {
  $locations = get_nav_menu_locations();
    $menuID = wp_get_nav_menu_object( $locations[ 'header_menu' ] ); // получаем ID
      $menu = wp_get_nav_menu_items($menuID);
      $object = get_queried_object();
      global $post;
      foreach ($menu as $i=>$item):
        $category[$i] = get_term_by('name', $item->title, 'category');
        $children[$i] = get_categories( array(
        'parent'    => $category[$i]->term_id,
        'hide_empty' => false
        ) );

        if(is_single()) {
          if (in_category($category[$i]->slug) ) {
            $class[$i] = 'active';
          }
        }

        if ($object->slug == $category[$i]->slug) {
          $class[$i] = 'active disabled';         
        }

        if (count($children[$i])) {
          foreach($children[$i] as $child) {
            if ( $object->slug == $child->slug  ) {
              $class[$i] = 'active';   
            }

            elseif (is_single() && in_category($child->slug) ) {
              $class[$i] = 'active';   
            }
          }
        }


      ?>
      
      <a href="<?=$item->url?>" class="<?=$class[$i]?>"><?=$item->title?></a>

      <?php
      endforeach;

}




function get_left_nav() {

  $locations = get_nav_menu_locations();

  $menuID = wp_get_nav_menu_object( $locations[ 'left_menu' ] ); // получаем ID

  $menu = wp_get_nav_menu_items($menuID);

  $close = 0;

  foreach ($menu as $item) {
    if (!$item->menu_item_parent) {
      $menuArray[$item->ID]['item'] = $item;
    }

    else {
      $menuArray[$item->menu_item_parent]['children'][] = $item;
    }
  }

  foreach ($menuArray as $menuItem):
    if ( count($menuItem['children']) ):
      ?>
    <div class="b-menu__nav__one">
      <ul>
      <li class="b-menu__nav__point"><a href="<?=$menuItem['item']->url?>"><?=$menuItem['item']->title?></a></li>
        <?php foreach ($menuItem['children'] as $child) :?>
        <li><a href="<?=$child->url?>"><?=$child->title?></a></li>
        <?php endforeach;?>
      </ul>
    </div>
    <?php else: ?>
        <?php if ($close == 0):?>
        <div class="br"></div>
        </div>
        <?php $close++; endif;?>

        <div class="b-menu__nav"><div class="b-menu__nav__one"><div class="b-menu__link b-menu__nav__point"><a href="<?=$menuItem['item']->url?>"><?=$menuItem['item']->title?></a></div></div></div><div class="br"></div>
    <?php
    endif;
  endforeach;

}

function get_footer_menu() {
	$locations = get_nav_menu_locations();

	$menuID = wp_get_nav_menu_object( $locations[ 'footer_menu' ] ); // получаем ID

	$menu = wp_get_nav_menu_items($menuID);

	$count = count($menu);

	foreach ($menu as $key => $item) {
		echo '<a href="'.$item->url.'">'.$item->title.'</a>' . "\n";

		if ($key < $count-1) echo '<span>&middot;</span>' . "\n";
	}
}


?>