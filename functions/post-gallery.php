<?php

function post_gallery_admin_scripts($hook) {
  global $post;

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
        if ( 'post' === $post->post_type ) { 
          
          wp_register_script('post-gallery', get_template_directory_uri().'/functions/post-gallery.js', array('jquery','media-upload','thickbox'), false, true);
          wp_enqueue_script('post-gallery');
        }
    }  
    
}
add_action('admin_enqueue_scripts', 'post_gallery_admin_scripts');

function gallery_admin_styles() {

    
    wp_register_style('post-gallery', get_template_directory_uri().'/functions/post-gallery.css');
    wp_enqueue_style('post-gallery');
}

add_action('admin_print_styles', 'gallery_admin_styles');



function gallery_add_custom_box() {
  $screens = array( 'post');
  foreach ( $screens as $screen )
    add_meta_box( 'post_gallery', 'Галереи изображений', 'post_gallery_meta_box_callback', $screen );
    
}

add_action('add_meta_boxes', 'gallery_add_custom_box');


function post_gallery_meta_box_callback() {
  ?>
  <div id="gallery__append">
  <?php
	
    global $post;
    $all = get_post_meta($post->ID, 'gallery__all', true);

    if (!$all):
    $all = 0;

    else:

    for ($y = 1; $y <= $all; $y++):

    $count[$y] = get_post_meta($post->ID, 'gallery' . $y . '__count', true);
  ?>
  
  <div class="gallery__item" data-gallery="<?=$y?>">
    <div class="gallery__heading" data-id="<?=$y?>">Галерея <?=$y?></div>
    <div class="gallery__code" data-id="<?=$y?>">[fotorama id="<?=$y?>"]</div>
    <div class="gallery__remove" data-remove="<?=$y?>">удалить галерею</div>
    <div class="gallery__container js-sortable" data-id="<?=$y?>">
  <?php
  if (!$count[$y]):
    $count[$y] = 0;

  else:
    for ($i=1; $i<=$count[$y]; $i++) :
    	$url[$y][$i] = wp_get_attachment_image_src(get_post_meta($post->ID, 'gallery' . $y . '__id' . $i, true));
  ?>
  		<div class="gallery__point" id="<?=$i?>">
  			<input type="hidden" class="gallery<?=$y?>__id" name="gallery<?=$y?>__id<?=$i?>" id="gallery<?=$y?>__id<?=$i?>" value="<?=get_post_meta($post->ID, 'gallery' . $y . '__id' . $i, true)?>" />
  			<img src="<?=$url[$y][$i][0]?>" class="gallery__image"/>
  			<span class="gallery__delete" data-id="<?=$y?>" data-delete="<?=$i?>">×</span>

  		</div>
    <?php
    endfor;
  endif;
  ?>
  
    </div>
    <a href="javascript:void(0)" class="gallery__add" data-id="<?=$y?>">Добавить изображение</a>
    <input type="hidden" name="gallery<?=$y?>__count" id="gallery<?=$y?>__count" value="<?php echo $count[$y];?>"/>
  </div>
  <?php endfor;endif; ?>
  
  </div>
  <input type="hidden" name="gallery__all" id="gallery__all" value="<?=$all?>">
  <a href="javascript:void(0)" class="gallery__new">Создать новую галерею</a>
  
  <?php
	
}


function gallery_save_postdata( $post_id ) {

    // проверяем, если это автосохранение ничего не делаем с данными нашей формы.
  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
  // Убедимся что поле установлено.
  if ( ! isset( $_POST['gallery__all'] ) )
    return;

  $all = $_POST['gallery__all'];
  update_post_meta( $post_id, 'gallery__all', $all );


  for ($y = 1; $y <= $all; $y++) {
      $count[$y] = $_POST['gallery' . $y . '__count'];

      for ($i=1; $i<=$count[$y]; $i++) {
        $id[$y][$i] = $_POST['gallery' . $y . '__id' . $i];

        update_post_meta( $post_id, 'gallery' . $y . '__id' . $i, $id[$y][$i] );
      }

      update_post_meta( $post_id, 'gallery' . $y . '__count', $count[$y] );
  }

  

}

add_action( 'save_post', 'gallery_save_postdata' );


function post_gallery_shortcode( $atts ) {
  global $post;
  $id = $atts['id'];


  if ($id) {
    $count = get_post_meta($post->ID, 'gallery' . $id . '__count', true);

    if ($count) {

      $return = '<div class="fotorama_post_container"><div class="b-article__image b-article__image_m">
        <div class="b-section__gallery">
          <div class="b-section__gallery__wrap">
            <div class="fotorama fotorama_gallery fotorama_post" data-width="100%" data-ratio="920/613" data-margin="0" data-nav="thumbs" data-thumbheight="42" data-fit="cover">';
             

            
      
      for ($i=1; $i<=$count; $i++) {
        $url[$i] = wp_get_attachment_image_src(get_post_meta($post->ID, 'gallery' . $id . '__id' . $i, true), 'large');
        $meta[$i] = wp_prepare_attachment_for_js(get_post_meta($post->ID, 'gallery' . $id . '__id' . $i, true));
        $caption[$i] = htmlspecialchars($meta[$i]['caption']);
        $descr[$i] = nl2br(htmlspecialchars($meta[$i]['description']));
        $return.= ' <img src="'.$url[$i][0].'" alt="" title="" width="700" height="467" data-description="'.nl2br($descr[$i]).'" data-cap="'.nl2br($caption[$i]).'">';
      }

      $return.= '
            </div>
          </div>
        </div>
      </div>
      
      <div class="b-article__important b-article__important_m ff-ss"><div class="js-fotorama-caption">'.nl2br($caption[1]).'</div>
      <div class="b-section__text__author b-section__text__author--fotorama ff-ss js-fotorama-description nom">
                '.nl2br($descr[1]).'
            </div>
      </div>
      </div>
       
      ';

      return $return;
    }

  }
}
add_shortcode( 'fotorama', 'post_gallery_shortcode' );

?>