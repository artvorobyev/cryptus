<?php

function highlight_results($content, $search = false){
	if (!$search) $search = get_search_query();
    $keys = implode('|', explode(' ', $search));
    $content = preg_replace('/(' . $keys .')/iu', '<span class="hl">\0</span>', $content);

    return $content;
}




function extract_search_content($h,$search = false,$w=15,$tag='span') {
	if (!$search) $search = get_search_query();
	$n = explode(" ",trim(strip_tags($search)));	//needles words
	$b = explode(" ",trim(strip_tags($h)));	//haystack words
	$c = array();						//array of words to keep/remove
	for ($j=0;$j<count($b);$j++) $c[$j]=false;
	for ($i=0;$i<count($b);$i++) 
		for ($k=0;$k<count($n);$k++) 
			if (stristr($b[$i],$n[$k])) {
				$b[$i]=preg_replace("/".$n[$k]."/i","<span class='hl'>\\0</span>",$b[$i]);
				for ( $j= max( $i-$w , 0 ) ;$j<min( $i+$w, count($b)); $j++) $c[$j]=true; 
			}	
	$o = "";	// reassembly words to keep
	for ($j=0;$j<count($b);$j++) if ($c[$j]) $o.=" ".$b[$j]; else $o.=".";
	$str = preg_replace("/\.{3,}/i","...",$o);
	$str = preg_replace("/\[([^\[\]]++|(?R))*+\]/", "", $str);
	return $str;
}


?>