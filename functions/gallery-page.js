var $=jQuery;

$(document).ready( function() {
	$( ".js-sortable" ).sortable();
	$( ".js-sortable" ).disableSelection();

	function recountIDs() {
		var array = [];
		var $images = $('.gallery__id');
		$images.each(function(index, item) {
			array.push($(item).val());
		});
		var result = array.join();
		$('#all_gallery_images').val(result);
	}


	$('.gallery__add').live('click', function() {
		var custom_uploader;

		if ( custom_uploader ) {
			custom_uploader.open();
			return;
		}

		custom_uploader = wp.media.frames.custom_uploader = wp.media({
			title: 'Выберите файл',
			button: {
				text: $( this ).data( 'uploader_button_text' )
			},
			multiple: true
		});

		custom_uploader.on('select', function() {
			var count = $('#gallery__count').val();
			count = parseInt(count);
			attachments = custom_uploader.state().get('selection').toJSON();
			$.each(attachments, function(i, attachment) {
				count++;
				var image = '<img src="' + attachments[i].sizes.thumbnail.url + '" class="gallery__image"/>';
				var result = '<div class="gallery__point" id="' + count + '">';
				result+= '<input type="hidden" class="gallery__id" name="gallery__id';
				result+= count;
				result+= '"';
				result+= ' id="gallery__id';
				result+= count;
				result+= '" value="';
				result+= attachments[i].id;
				result+= '" />';
				result+= image;
				result+= '<span class="gallery__delete" data-delete="';
				result+= count;
				result+= '">×</span>';
				$('.gallery__container').append(result);
				$('#gallery__count').val(count);
			});
			recountIDs();

		});

		custom_uploader.open();
	});


	$('.gallery__delete').live('click', function() {
		var id = ($(this).attr('data-delete'));
		var count = parseInt($('#gallery__count').val());
		
		if (id < count) {
			$(this).closest('.gallery__container').find('.gallery__point').each(function(index, element) {
				var new_id = parseInt($(element).attr('id'));
				if (new_id > id) {
					var var_id = parseInt(new_id - 1);
					$(element).attr('id', var_id);
					$(element).find('.gallery__delete').first().attr('data-delete', var_id);
					$(element).find('input').each(function() {
						var el_class = $(this).attr('class');
						var el_name = el_class + var_id;
						$(this).attr('name', el_name);
						$(this).attr('id', el_name)
					});
				}
			});
		}

		$(this).closest('.gallery__point').remove();

		var new_count = parseInt(count) - 1;
		$('#gallery__count').val(new_count);
		recountIDs();
	});

	$('.gallery__container').bind( "sortstop", function(event, ui) {
		$(this).find('.gallery__point').each(function(index, element) {
			var id = index + 1
			$(this).attr('id', id);
			$(this).find('.gallery__delete').first().attr('data-delete', id);
			$(this).find('input, textarea').each(function() {
				var el_class = $(this).attr('class');
				var el_name = el_class + id;
				$(this).attr('name', el_name);
				$(this).attr('id', el_name)
			})
		});
		recountIDs();
	});

});