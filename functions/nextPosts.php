<?php

function loadNextPost() {

	$article = intval($_POST['article']);

	$startPost = get_post(intval($_POST['startPost']));

	$events = get_category_by_slug('events');

	$posts = new wp_query(array('posts_per_page'=>1, 'post_status'=>'publish', 'paged' => $article, 'category__not_in' => array($events->term_id),
		'date_query' => array(
			'before' => $startPost->post_date
			)
		));

	while ($posts->have_posts()): $posts->the_post();

		$html = '<div class="loading-shadow"></div> <div class="loading opacity"> <div class="l-wrap l-wrap--article l-wrap--uploading"> <div class="b-article">';
		if (get_post_meta(get_the_ID(), 'is_partner', true) == '1'):
		$html.= '<div class="b-article__source"> <div class="b-article__source__left">Партнерский материал</div>';
			if (get_post_meta(get_the_ID(), 'partner_logo', true)):
				$html.= '<div class="b-article__source__logo">';
					if (get_post_meta(get_the_ID(), 'partner_url', true)):
						$html.= '<a href="' . get_post_meta(get_the_ID(), 'partner_url', true) . '">';
					endif;

					$html.= '<img src="' . wp_get_attachment_image_src(get_post_meta(get_the_ID(), 'partner_logo', true), '', true)[0].'" alt="" title="" height="18">';
					if (get_post_meta(get_the_ID(), 'partner_url', true)):
						$html.= '</a>';
					endif;

					$html.='</div>';
				endif;

				$html.='<div class="br"></div></div>';
		endif;

		$html.= '<div class="b-article__title"><h1 class="h1">' . get_the_title().'</h1></div> <div class="b-article__pretext">'.get_subheading().'</div> <div class="b-article__info b-article__info_top ff-ss"> <div class="b-article__info__left b-article__info__left_nopad"> '.get_norm_date().', '.get_the_time('G:i').'<span>&middot;</span>';
		$html.= get_norm_cats_no_print();
		$html.='</div> <div class="b-article__info__right"> <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script> <script src="//yastatic.net/share2/share.js"></script> <div class="ya-share2" data-services="facebook,vkontakte,twitter,odnoklassniki" data-counter="" id="shareTop'.$article.'"></div> </div> <div class="br"></div> </div>';
	    if (intval(get_post_meta(get_the_ID(), 'is_banner', true))):
		    $banner_class = ' b-article__content_nobanner';
		    $banner_image_class = ' b-article__image_wide';

	    else:

	    	$html.= '<div class="b-article__banner">'.get_banner_by_slug('article','1v').'</div>';

	    endif;

	    $html.= '<div class="b-article__content '.$banner_class.'">';

	    $meta = wp_prepare_attachment_for_js(get_post_thumbnail_id($post));

	    if (intval(get_post_meta(get_the_ID(), 'is_show_thumb', true)) != 1):
		    $html.= ' <div class="b-article__image '.$banner_image_class.'"> <img src="'.get_thumb_url('cryptusHuge').'" alt="'.get_thumb_caption().'" title="" width="700" height="467"> <div class="b-section__text ff-ss"> ';
		    $html.= '<div class="b-section__text__descr">'.$meta['caption'].'</div>';

		    if ($meta['description']):
		        $html.='<div class="b-section__text__author">'.$meta['description'].'</div>';
		    endif;

		    $html.='</div> </div>';
	    endif;


	    if (get_post_meta(get_the_ID(), 'lead', true)):

	        $html.= '<div class="b-article__important ff-ss">'.wpautop(get_post_meta(get_the_ID(), 'lead', true)).'</div>';

	    endif;

	    $html.='<div class="b-article__wrap">'.get_the_content();


        if (intval(get_post_meta(get_the_ID(), 'is_show_thumb', true)) && $meta['description']):

                            
        $html.='<div class="b-section__text ff-ss" style="border-bottom: none; padding: 30px 0 0;">

                               <div class="b-section__text__author">Источник изображения в анонсе: ' . str_replace('Фото: ','',$meta['description']) . '</div>
                           
                            </div>';
                        
        endif;

	    $html.='</div> </div> <div class="br"></div> <div class="b-article__info b-article__info_bottom b-article__info_tagged ff-ss"> <div class="b-article__info__left">';

	    if (intval(get_post_meta(get_the_ID(), 'show_author', true))):
	        $html.= '<a href="'.get_author_posts_url( get_the_author_meta( 'ID' )).'" class="b-article__info__author"> <span class="b-article__info__author__image">';
	        if (get_author_photo(get_the_author_meta( 'ID' ))):
	            $html.='<img src="'.get_author_photo(get_the_author_meta( 'ID' )).'" alt="" title="" width="50" height="50">';
	        endif;

	    	$html.= '</span>'.get_the_author().'</a><span>&middot;</span>';
	    endif;

	    $html.=get_norm_cats_no_print();

	    $html.='<span>&middot;</span>'.get_norm_date().', '.get_the_time('G:i').'</div> <div class="b-article__info__right b-article__info__right_circles"> <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script> <script src="//yastatic.net/share2/share.js"></script> <div class="ya-share2" data-services="facebook,vkontakte,twitter,odnoklassniki" data-counter="" id="shareBottom'.$article.'"></div> </div> <div class="br"></div> </div>';
	    if (has_tag()):

	        $html.='<div class="b-article__tags ff-ss">'.get_norm_tags_no_print().'</div>';

	    endif;

	    $html.='</div> <div class="spacer"></div> '; 

	    $html.= load_template_part('readmore');



	    if (!intval(get_post_meta(get_the_ID(), 'is_banner', true))):
	    	$html.='<div class="spacer"></div> <div class="banner">'.get_banner_by_slug('article','2h').'</div>'; 
	    endif;

	    $html.='<div class="spacer js-last"></div> ';

	    if ($posts->found_posts == $article) $html.= '<div class="spacer"></div><div class="spacer"></div><div class="spacer"></div> ';

	    $html.=' </div> </div>'; 


	    


	    

	    $send['data']['title'] = get_the_title();
	    $send['data']['subheading'] = get_subheading();
	    $send['data']['permalink'] = get_the_permalink();

	    if (intval(get_post_meta(get_the_ID, 'ogImage', true))):
	    $send['data']['opengraph_image'] = wp_get_attachment_image_src(get_post_meta(get_the_ID, 'ogImage', true), false, true)[0];
		else: 
			$send['data']['opengraph_image'] = '';
		endif;

	    $send['html'] = $html;

    endwhile;

    wp_send_json($send);

}

add_action('wp_ajax_nopriv_loadNextPost', 'loadNextPost'); 
add_action('wp_ajax_loadNextPost', 'loadNextPost');


function loadStartPostData() {
	global $post;
	$post_id = intval($_POST['startPost']);
	$post = get_post($post_id);
	setup_postdata($post);
	$send['title'] = get_the_title();
	$send['subheading'] = get_subheading();
	$send['permalink'] = get_the_permalink();
	
	if (intval(get_post_meta(get_the_ID, 'ogImage', true))):
	    $send['opengraph_image'] = wp_get_attachment_image_src(get_post_meta(get_the_ID, 'ogImage', true), false, true)[0];
	else: 
		$send['opengraph_image'] = '';
	endif;

	wp_send_json($send);
}

add_action('wp_ajax_nopriv_loadStartPostData', 'loadStartPostData'); 
add_action('wp_ajax_loadStartPostData', 'loadStartPostData');


?>