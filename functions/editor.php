<?php

add_action('init', 'set_editor_styles');
function set_editor_styles() {
  add_editor_style('/functions/editor.css');
}


// Editor settings




add_action( 'init', 'wptuts_buttons' );

function wptuts_buttons() {
  add_filter("mce_external_plugins", "wptuts_add_buttons");
} 
function wptuts_add_buttons($plugin_array) {
  $plugin_array['wptuts'] = get_template_directory_uri() . '/functions/editor.js?v=' . get_file_ver('/functions/editor.js');
  return $plugin_array;
}


add_filter('tiny_mce_before_init', 'set_default_editor_settings');
function set_default_editor_settings($init) {
  $init['paste_remove_styles'] = true;
  $init['paste_remove_spans'] = true;
  $init['paste_strip_class_attributes'] = 'all';



  return $init;
}


function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');











add_filter('tiny_mce_before_init', 'oly_set_default_editor_settings');
function oly_set_default_editor_settings($init) {
  global $post;
    if  (in_array($post->post_type, array('post', 'page') ) ) {
      $init['paste_remove_styles'] = true;
      $init['paste_remove_spans'] = true;
      $init['paste_strip_class_attributes'] = 'all';
      $init['body_class'] = 'entry fonts-loaded';
      //$init['plugins'] = 'table';
      //$init['menubar'] = 'table';
      //$init['wordpress_adv_hidden'] = FALSE;


      // Buttons
      $init['toolbar1'] = 'formatselect,undo,redo,|,bold,italic,|,bullist,numlist,|,link,unlink,|,table,|,article_quote,lead,factoid,factoid_r,youtube,article_frame,article_image,article_line,article_line_arrow,article_line_number,|,pastetext,removeformat,|,fullscreen ';
      $init['toolbar2'] = '';
    }


  return $init;
}

function RemoveAddMediaButtonsForNonAdmins(){
   global $post;
    if ($post->post_type == 'post' ) {
        remove_action( 'media_buttons', 'media_buttons' );
    }
}
add_action('admin_head', 'RemoveAddMediaButtonsForNonAdmins');

function remove_empty_p( $content ) {
    $content = force_balance_tags( $content );
    $content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
    $content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
    return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);
?>