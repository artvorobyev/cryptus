function youtube_parser(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11)? match[7] : false;
}

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

(function() {
    tinymce.create('tinymce.plugins.WPTuts', {
        /**
         * Initializes the plugin, this will be executed after the plugin has been created.
         * This call is done before the editor instance has finished it's initialization so use the onInit event
         * of the editor instance to intercept that event.
         *
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {


            ed.addCommand('lead', function() {
                var selected_text = ed.selection.getContent();
                var return_text = '';
                return_text = ' <div class="b-article__important b-article__important_m ff-ss">' + selected_text + '</div><p></p>';
                ed.execCommand('mceInsertContent', 0, return_text);
            });

            ed.addCommand('article_line', function() {
                var return_text = ' <div class="b-article__line"></div><p></p>';
                ed.execCommand('mceInsertContent', 0, return_text);
            });

            ed.addCommand('article_line_arrow', function() {
                var return_text = ' <div class="b-article__line b-article__line_arrow"></div><p></p>';
                ed.execCommand('mceInsertContent', 0, return_text);
            });

            ed.addCommand('article_line_number', function() {
                var selected_text = ed.selection.getContent();
                ed.windowManager.open( {
                    title: 'Вставить разделитель с цифрой',
                    width:800,
                    height:250,
                    body: [{
                        type: 'textbox',
                        name: 'text',
                        label: 'Цифра (максимум 2 символа)',
                        value: '20'
                    }],
                    onsubmit: function( e ) {
                        var expert_text = '<div class="b-article__line b-article__line_num"><span class="b-article__line__num">' + e.data.text + '</span></div><p></p>';
                        ed.execCommand('mceInsertContent', 0, expert_text);
                    }
                });
                
            });



            ed.addCommand('factoid', function() {
                var selected_text = ed.selection.getContent();
                var return_text = '';
                return_text = ' <div class="b-factoid ff-ss">' + selected_text + '</div><p></p>';
                ed.execCommand('mceInsertContent', 0, return_text);
            });

            ed.addCommand('factoid_r', function() {
                var selected_text = ed.selection.getContent();
                ed.windowManager.open( {
                    title: 'Вставить блок с фактоидом',
                    width:800,
                    height:250,
                    body: [{
                        type: 'textbox',
                        name: 'text',
                        label: 'Текст слева',
                        multiline: true,
                        value: selected_text
                    },
                    {
                        type: 'textbox',
                        name: 'factoid',
                        multiline: true,
                        label: 'Фактоид'
                    }],
                    onsubmit: function( e ) {
                        var expert_text = '<div class="b-factoid-block"><div class="b-factoid ff-ss">'+ e.data.factoid + '</div><p>' + e.data.text + '</p></div><p></p>';
                        ed.execCommand('mceInsertContent', 0, expert_text);
                    }
                });
                
            });

            ed.addCommand('youtube', function() {
                var selected_text = ed.selection.getContent();
                ed.windowManager.open( {
                    title: 'Вставить видео с Ютуба',
                    width:800,
                    height:250,
                    body: [{
                        type: 'textbox',
                        name: 'url',
                        label: 'Ссылка на видео'
                    },
                    {
                        type: 'textbox',
                        name: 'text',
                        multiline: true,
                        label: 'Подпись'
                    },
                    {
                        type: 'textbox',
                        name: 'author',
                        multiline: true,
                        label: 'Авторство'
                    }],
                    onsubmit: function( e ) {
                        var link = youtube_parser(e.data.url);
                        var expert_text = '<div class="b-article__image b-article__image_video"> <iframe width="580" height="325" src="https://www.youtube.com/embed/'+link+'" frameborder="0" allowfullscreen></iframe> <div class="b-section__text ff-ss"> <div class="b-section__text__descr">'+e.data.text+'</div> <div class="b-section__text__author">'+e.data.author+'</div> </div> </div><p></p>'; ed.execCommand('mceInsertContent', 0, expert_text);
                    }
                });
                
            });


            ed.addCommand('article_frame', function() {
                var selected_text = ed.selection.getContent();
                ed.windowManager.open( {
                    title: 'Вставить фрейм с внешнего сервиса',
                    width:800,
                    height:250,
                    body: [{
                        type: 'textbox',
                        name: 'frame',
                        multiline: true,
                        label: 'Код фрейма'
                    },
                    {
                        type:'container',
                        name:'container',
                        html:'При вставке видео проверить width="580" height="325"'

                    }],
                    onsubmit: function( e ) {
                        var expert_text = '<div class="b-article__image b-article__image_video">'+e.data.frame+'</div><p></p>'; ed.execCommand('mceInsertContent', 0, expert_text);
                    }
                });
                
            });



            ed.addCommand('article_image', function() {
                var selected_text = ed.selection.getContent();
                ed.windowManager.open( {
                    title: 'Вставить изображение',
                    width:800,
                    height:250,
                    body: [{
                        type: 'textbox',
                        name: 'image',
                        label: 'URL-адрес изображения',
                        id: 'urlimg'
                    },
                    ,
                    {
                        type: 'button',
                        name: 'select-file',
                        text: 'Выбрать из библиотеки',
                        onclick: function() {
                            window.mb = window.mb || {};

                            window.mb.frame = wp.media({
                                library : {
                                    type : 'image'
                                },
                                multiple: false
                            });

                            window.mb.frame.on('select', function() {
                                var json = window.mb.frame.state().get('selection').first().toJSON();

                                if (json.length != 0) {
                                    jQuery('#urlimg').val(json.url);
                                    jQuery('#my_caption_img').val(json.caption);
                                    jQuery('#my_author_img').val(json.description);
                                }
                            });

                            window.mb.frame.open();
                        }
                    },{
                        type: 'textbox',
                        name: 'text',
                        multiline: true,
                        label: 'Подпись',
                        id: 'my_caption_img'
                    },
                    {
                        type: 'textbox',
                        name: 'author',
                        multiline: true,
                        label: 'Авторство',
                        id: 'my_author_img'
                    }],
                    onsubmit: function( e ) {

                        if (e.data.text.length>0 || e.data.author.length>0) {
                            var image_text = '<div class="b-section__text ff-ss">';

                            if (e.data.text.length>0) {
                                image_text+= '<div class="b-section__text__descr">';
                                image_text+= e.data.text;
                                image_text+= '</div>';
                                
                            }

                            if (e.data.author.length>0) {
                                image_text+= '<div class="b-section__text__author">';
                                image_text+= e.data.author;
                                image_text+= '</div>'; 
                            
                            }

                            image_text+= '</div>';
                        }

                        else {
                            var image_text = '';
                        }
                        
                        var expert_text = '<div class="b-article__image"> <img src="'+e.data.image+'" alt="'+escapeHtml(e.data.text)+'"> '+image_text+' </div><p></p>';
                        ed.execCommand('mceInsertContent', 0, expert_text);
                    }
                });
                
            });


            
            ed.addCommand('article_quote', function() {
                var selected_text = ed.selection.getContent();
                ed.windowManager.open( {
                    title: 'Вставить цитату',
                    width:800,
                    height:250,
                    body: [{
                        type: 'textbox',
                        name: 'text',
                        label: 'Текст цитаты',
                        multiline: true,
                        id:'my-editor-textarea',
                        value: selected_text
                    },
                    {
                        type: 'textbox',
                        name: 'name',
                        label: 'Имя автора'
                    },
                    {
                        type: 'textbox',
                        name: 'info',
                        label: 'Информация об авторе'
                    }],
                    onsubmit: function( e ) {
                        var info = '';
                        if (e.data.name.length > 0 || e.data.info.length > 0){

                            info+= '<div class="b-quote__author"><b>';
                            info+= e.data.name;
                            info+= '</b>, ';
                            info+= e.data.info;
                            info+= '</div>';


                        }
                        var text = e.data.text;
                        var expert_text = '';
                        expert_text = '<div class="b-quote">' + text + info + '</div><p></p>';
                        ed.execCommand('mceInsertContent', 0, expert_text);
                    }
                });
                
            });
            
            





            ed.addButton('lead', {
                title : 'Лид',
                cmd : 'lead',
                icon : 'icon dashicons-before dashicons-format-aside'
            });
			
			ed.addButton('article_quote', {
                title : 'Цитата',
                cmd : 'article_quote',
                icon : 'icon dashicons-before dashicons-format-quote'
            });
            


            ed.addButton('factoid', {
                title : 'Фактоид большой',
                cmd : 'factoid',
                icon : 'icon dashicons-before dashicons-align-none'
            });

            ed.addButton('factoid_r', {
                title : 'Фактоид маленький',
                cmd : 'factoid_r',
                icon : 'icon dashicons-before dashicons-align-right'
            });

            ed.addButton('youtube', {
                title : 'Видео с Ютуба',
                cmd : 'youtube',
                icon : 'icon dashicons-before dashicons-video-alt3'
            });

            ed.addButton('article_frame', {
                title : 'Фрейм с внешнего сервиса',
                cmd : 'article_frame',
                icon : 'icon dashicons-before dashicons-format-video'
            });

            ed.addButton('article_image', {
                title : 'Изображение',
                cmd : 'article_image',
                icon : 'icon dashicons-before dashicons-format-image'
            });

            ed.addButton('article_line', {
                title : 'Разделитель',
                cmd : 'article_line',
                icon : 'icon dashicons-before dashicons-minus'
            });

            ed.addButton('article_line_arrow', {
                title : 'Разделитель со стрелкой',
                cmd : 'article_line_arrow',
                icon : 'icon dashicons-before dashicons-arrow-down-alt'
            });

            ed.addButton('article_line_number', {
                title : 'Разделитель с цифрой',
                cmd : 'article_line_number',
                icon : 'icon dashicons-before dashicons-marker'
            });
            
            
        },

        /**
         * Creates control instances based in the incomming name. This method is normally not
         * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
         * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
         * method can be used to create those.
         *
         * @param {String} n Name of the control to create.
         * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
         * @return {tinymce.ui.Control} New control instance or null if no control was created.
         */
        createControl : function(n, cm) {
            return null;
        },

        /**
         * Returns information about the plugin as a name/value array.
         * The current keys are longname, author, authorurl, infourl and version.
         *
         * @return {Object} Name/value array containing information about the plugin.
         */
        getInfo : function() {
            return {
                    longname : 'WPTuts Buttons',
                    author : 'Lee',
                    authorurl : 'http://wp.tutsplus.com/author/leepham',
                    infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/example',
                    version : "0.1"
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add('wptuts', tinymce.plugins.WPTuts);
})();