jQuery(function($) {
	function createOpengraphImage() {
		if (window.innerWidth < 1400) {
			$('body').addClass('hugeBody1500');
		}

		
		html2canvas($("#ogDiv"), {
				onrendered: function(canvas) {
					theCanvas = canvas;

					
					
					document.body.appendChild(canvas);
					if (window.innerWidth < 1400) {
						canvas.scrollIntoView();
					}

	                // Convert and download as image 
	                //Canvas2Image.saveAsPNG(canvas); 
	                //$("#ogAppend").append(canvas);
	                // Clean up 
	                //document.body.removeChild(canvas);
	                
	                $.post(ajaxUrl, {
	                	action: 'ogImageCreate',
	                	id: id,
	                	image: canvas.toDataURL('image/png')
	                }).success(function(response) {
	                	$("#ogAppend").html('<img src="'+response.url+'"/>');
	                	$('#ogImage').val(response.id);
	                	$('#ogLoading').hide();
	                	$('#publish').click();
	                	$('body').removeClass('hugeBody1500');
	                });
	            }
	        });

	}



	$(document).ready(function() {

		var text = $('#publish').val();

		$('#publish').hide();
		$('#publishing-action').append('<span id="publishFake" class="button button-primary button-large">'+text+'</span>');

		canvg();

		$('body').on('click', '#publishFake', function() {



			

			$('#ogLoading').show();
			$('#publishing-action .spinner').css('visibility', 'visible');

			$('#publishFake').addClass('disabled');

			$('#ogDiv').removeClass('cover');
			$('#ogDiv').css('background-image', 'none');

			var title = $('#title').val();
			$("#ogText").html(title);

			var thumbnailId = $('#_thumbnail_id').val();

			if (parseInt(thumbnailId) > 0) {
				$.post(ajaxUrl, {
					action: 'getThumbnailUrlById',
					thumbnailId: thumbnailId
				}).success(function(url) {
					$('#ogDiv').css('background-image', 'url(' + url + ')');
					$('#ogDiv').addClass('cover');
				}).done(function() {
					createOpengraphImage();
				});
			}

			else {
				createOpengraphImage();
			}



			

			
			
		});
	});
});