<?php 


add_action('init', 'quotes_custom_post_types');
function quotes_custom_post_types() {

	global $wp_rewrite;

	register_post_type('quote', array(
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'publicly_queryable' => true,
		'menu_position' => 10,
		'supports' => array('title', 'thumbnail'),
		'labels' => 
		    array(
		        'name' => 'Цитаты',
		        'singular_name' => 'Цитата',
		        'add_new' => 'Добавить цитату',
		        'add_new_item' => 'Добавить цитату',
		        'edit_item' => 'Редактировать цитату',
		        'new_item' => 'Новая цитата',
		        'all_items' => 'Все цитаты',
		        'view_item' => 'Просмотр цитаты',
		        'search_items' => 'Искать цитату',
		        'not_found' =>  'Не найдено цитат',
		        'not_found_in_trash' => 'Не найдено цитат в корзине', 
		        'parent_item_colon' => ''
	        ),
	    'has_archive' => true,
		'rewrite' => true,
		'menu_icon' => 'dashicons-format-quote'
	));

	register_post_type('number', array(
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'publicly_queryable' => true,
		'menu_position' => 11,
		'supports' => array('title'),
		'labels' => 
		    array(
		        'name' => 'Цифры',
		        'singular_name' => 'Цифра',
		        'add_new' => 'Добавить цифру',
		        'add_new_item' => 'Добавить цифру',
		        'edit_item' => 'Редактировать цифру',
		        'new_item' => 'Новая цифра',
		        'all_items' => 'Все цифры',
		        'view_item' => 'Просмотр цифры',
		        'search_items' => 'Искать цифру',
		        'not_found' =>  'Не найдено цифр',
		        'not_found_in_trash' => 'Не найдено цифр в корзине', 
		        'parent_item_colon' => ''
	        ),
	    'has_archive' => true,
		'rewrite' => true,
		'menu_icon' => 'dashicons-editor-aligncenter'
	));
}





function facts_custom_boxes() {
	$screens = array( 'number', 'quote');
	global $post;
	foreach ( $screens as $screen ) {

		add_meta_box( 'fact_text', 'Текст', 'fact_text_meta_box_callback', $screen);
		add_meta_box( 'fact_link', 'Ссылка', 'fact_link_meta_box_callback', $screen);
	
	}

	add_meta_box( 'quote_author', 'Автор цитаты', 'quote_author_meta_box_callback', 'quote');
	add_meta_box( 'quote_job', 'Описание автора цитаты', 'quote_job_meta_box_callback', 'quote');
}

add_action('add_meta_boxes', 'facts_custom_boxes');




function fact_text_meta_box_callback() {
		global $post;
	?>
		<textarea style="width:100%; height:100px;" name="fact_text"><?=get_post_meta($post->ID, 'fact_text', true)?></textarea>
	<?php
}

function fact_link_meta_box_callback() {
		global $post;
	?>
		<input type="text" style="width:100%;" name="fact_link" value="<?=get_post_meta($post->ID, 'fact_link', true)?>"/>
	<?php
}


function quote_author_meta_box_callback() {
		global $post;
	?>
		<input type="text" style="width:100%;" name="quote_author" value="<?=get_post_meta($post->ID, 'quote_author', true)?>"/>
	<?php
}


function quote_job_meta_box_callback() {
		global $post;
	?>
		<input type="text" style="width:100%;" name="quote_job" value="<?=get_post_meta($post->ID, 'quote_job', true)?>"/>
	<?php
}












function facts_text_save_postdata( $post_id ) {


	$array = array('fact_text','fact_link','quote_author','quote_job');

	foreach ($array as $item) {
		if (isset($_REQUEST[$item])):
			update_post_meta( $post_id, $item, htmlspecialchars($_POST[$item]) );
		endif;
	}
	

		
}

add_action( 'save_post', 'facts_text_save_postdata' );






function hide_quote_slug() {
	global $post;

	if (in_array($post->post_type,array('quote','number'))):
	?>
		<style>
			#edit-slug-box {
				display: none;
			}
		</style>
	<?php
	endif;
}

add_action('admin_head', 'hide_quote_slug');


?>