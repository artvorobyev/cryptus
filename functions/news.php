<?php

function get_news($date) {

	$args = array(
		'category_name' => 'events',
		'date_query' => array(
			'year' => $date->format('Y'),
			'month' => $date->format('n'),
			'day' => $date->format('j')
			),
		'posts_per_page' => -1,
		'post_status'=>'publish'
		);

	$posts = new wp_query($args);

	if ($posts->have_posts()):
	

	$result = '
	<div class="b-list__section">
	<div class="b-list__section__title">'.filter_date($date->format('j %m')).'</div>
	<div class="b-list__section__items">';



		while ($posts->have_posts()):
			$posts->the_post();

			if (has_post_thumbnail()):

			$result.= '
			<div class="b-list__section__items__one">
				<div class="b-list__section__items__one__image"><a href="'.get_the_permalink().'"><img src="'.get_thumb_url("cryptusSmall").'" alt="" title="" width="270" height="178"></a></div>
				<div class="b-list__section__items__one__text">
					<a href="'.get_the_permalink().'">'.get_the_title().'</a>
					<div class="b-list__section__items__one__date date">'.get_norm_date(false).', '.get_the_time('G:i').'</div>
				</div>
				<div class="br"></div>
			</div>';


			else:
			$result.= '<div class="b-list__section__items__one">
				<a href="'.get_the_permalink().'">'.get_the_title().'</a>
				<div class="b-list__section__items__one__date date">'.get_norm_date().', '.get_the_time('G:i').'</div>
			</div>';

	endif; endwhile;

			
	$result.= '
	</div>
	<div class="br"></div>
	</div>';

	return $result;

	endif;
}


function news_ajax() {
	if (isset($_POST['page'])) {
		$page = $_POST['page'];

		for ($i = -2; $i < 0; $i++) {
			$day = (-1)*(intval($page)*2+$i); 
			$date = new DateTime(date('d.m.Y'));
			$date->modify($day . ' day');
			if ( get_news($date) ) {
				$send[] = get_news($date);
			}

			else {
				$send[] = 'end';
			}

			
		}

		wp_send_json($send);
	}
}

add_action('wp_ajax_nopriv_news_ajax', 'news_ajax'); 
add_action('wp_ajax_news_ajax', 'news_ajax');

?>