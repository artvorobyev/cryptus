var $ = jQuery;

$(document).ready(function() {
	$('#is_partner').on('change', function() {
		if ($(this).is(':checked')) {
			$('#partner_elements').slideDown(200);
		}

		else {
			$('#partner_elements').slideUp(200);
		}
	});

	$('.logo__choose').on('click', function() {
		var file_frame;
		var obj = this;

		if ( file_frame ) {
			file_frame.open();
			return;
		}

		file_frame = wp.media.frames.file_frame = wp.media({
			title: 'Выберите файл',
			button: {
				text: $( this ).data( 'uploader_button_text' )
			},
			multiple: false
		});

		file_frame.on('select', function() {
			attachment = file_frame.state().get('selection').first().toJSON();
			var image = '<img src="' + attachment.url + '" id="partner_image"/>';
			$('#partner_here').prepend(image);
			$('#partner_logo').val(attachment.id);
			$(obj).hide();
			$('.logo__delete').show();
		});

		file_frame.open();
	});

	$('.logo__delete').on('click', function() {
		$('#partner_image').remove();
		$('#partner_logo').val('');
		$('.logo__choose').show();
		$(this).hide();
	});

	$('[name="subheading"], #title').keyup(function () {
	  if ($(this).is('#title') ) {
	  	var max = 65;
	  }
	  else {
	  	var max = 100;
	  }
	  var len = $(this).val().length;
	  var $controller = $(this).closest('div').find('.js-controller').first();
	  if (len >= max) {
	     $(this).val($(this).val().substring(0, len-1));
	     $controller.addClass('red');
	  }

	  else {
	  	$controller.removeClass('red');
	  }

	  var char = max - len;
	  $controller.text(char);

	});

	$('#titlewrap').append('<div class="js-controller">65</div>').after(function() {
		$('.js-controller').each(function(i, item) {
			var text = parseInt($(item).text());
			var len = $(item).parent().find('input').val().length;
			$(item).text(text-len);
		});
	});

	

	




});

