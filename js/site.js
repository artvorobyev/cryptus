/* get viewport width */
function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}

/* auto-height */
var ids = [];
function blockHeight(ids){
    $.each(ids, function(i, id){
        $('.js-height[data-height="'+id+'"]').height('auto').removeClass('heighted');
        var h = 0;
        $('.js-height[data-height="'+id+'"]').each(function(){
            if($(this).outerHeight() > h){
                h = $(this).outerHeight();
            }
        });
        $('.js-height[data-height="'+id+'"]').outerHeight(h).addClass('heighted');
    });
}

/* masked classremover - $('div').removeClasses('status_*'); */
(function($) {
    $.fn.removeClasses = function(mask) {
        return this.removeClass(function(index, cls) {
            var re = mask.replace(/\*/g, '\\S+');
            return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
        });
    };
})($);

function replaceSVG() {
    $('img.svg').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');
            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            // Replace image with new SVG
            $img.replaceWith($svg);
        }, 'xml');
    });
}

$(document).ready(function () {

    $('.js-height').each(function () {
        var id = $(this).attr('data-height');
        if ($.inArray(id, ids)) {
            ids[ids.length] = id;
        }
    });
    blockHeight(ids);

    $('.js-menu-open').click(function () {
        $('#fader').fadeIn(500);
        $('.b-menu').addClass('active');
        return false;
    });

    $('.js-menu-close, #fader').click(function () {
        $('.b-menu').removeClass('active');
        $('#fader').fadeOut(500);
        return false;
    });

    $('.js-search-open').click(function () {
        $('.js-search-form').addClass('active');
    });

    $('.js-search-close').click(function () {
        $('.js-search-form').removeClass('active');
        setTimeout(function () {
            $('.js-search-field').val(' ');
        }, 500);
    });

});

$(window).resize(function () {
    blockHeight(ids);

    $('.js-scrollbar').each(function () {
        $(this).data('plugin_tinyscrollbar').update();
    });
});

$(window).load(function () {
    blockHeight(ids);

    $(window).resize();

    $('.js-scrollbar').each(function () {
        $(this).data('plugin_tinyscrollbar').update();
    });
});

$(document).ready(function() {
    $('.b-article__wrap table:not(".table")').addClass('table');
    $('.b-article__wrap table thead tr').addClass('nohover');
    $('.b-article__wrap table thead td').each(function(i, item) {
        var html = $(item).html();
        $(item).parent().append('<th>'+html+'</th>');
        $(item).remove();
    });

    /* fotorama --- html description at custom position */
    $('.fotorama_gallery').on('fotorama:show ', function (e, fotorama, extra) {
        var f = this;

        if ($(f).hasClass('fotorama_post')) {
            var p = $(f).closest('.fotorama_post_container');
            $(p).find('.js-fotorama-description').stop().fadeTo(500, 0, function () {
                var desc = fotorama.activeFrame.description ? fotorama.activeFrame.description : '';
                $(this).html(desc).fadeTo(500, 1);
            });

            if ( $(p).find('.js-fotorama-caption').length > 0 ) {
                $(p).find('.js-fotorama-caption').stop().fadeTo(500, 0, function () {
                    var caption = fotorama.activeFrame.cap ? fotorama.activeFrame.cap : '';
                    $(this).html(caption).fadeTo(500, 1);
                });
            }
        }

        else {
            $('.js-fotorama-description').stop().fadeTo(500, 0, function () {
                var desc = fotorama.activeFrame.description ? fotorama.activeFrame.description : '';
                $(this).html(desc).fadeTo(500, 1);
            });

            if ( $('.js-fotorama-caption').length > 0 ) {
                $('.js-fotorama-caption').stop().fadeTo(500, 0, function () {
                    var caption = fotorama.activeFrame.cap ? fotorama.activeFrame.cap : '';
                    $(this).html(caption).fadeTo(500, 1);
                });
            }
        }
        
        
    });
});

$(document).ready(function() {
    var page = 2;
    var $button = $('#newsMore');
    $button.on('click', function() {
        var text = $button.text();
        $button.text('Загружаем...');
        $.post(ajaxUrl, {
            action: 'news_ajax',
            page: page
        }).success(function(posts) {
            $.each(posts, function(i, item) {
                if (item == 'end') {
                    if (posts[i+1] == 'end') {
                        $button.remove();
                    }
                    
                }

                else {
                    $('#newsLoad').append(item);
                    page++;
                    $button.text(text);
                }
            });
            
        });
    });
});


$(document).ready(function() {
    var authorPage = 2;
    var $authorButton = $('#authorMore');
    $authorButton.on('click', function() {
        var text = $authorButton.text();
        var authorID = $(this).data('id');
        var type = $(this).data('type');
        $authorButton.text('Загружаем...');

        if (type == 'search') {
            var action = 'search_results_ajax';
        }

        else {
            var action = 'author_ajax';
        }

        $.post(ajaxUrl, {
            action: action,
            page: authorPage,
            type: type,
            id: authorID
        }).success(function(posts) {
                var html = $.parseHTML(posts);
                var len = $(html).find('.b-section__one').size();

                if (posts == 'end') {
                    $authorButton.remove();
                 
                }

                else {
                    $('#loadHere').append(posts);
                    $('#loadHere').imagesLoaded( function() {

                        $('.js-height').each(function () {
                            var id = $(this).attr('data-height');
                            if ($.inArray(id, ids)) {
                                ids[ids.length] = id;
                            }
                        });
                        blockHeight(ids);
                    
                
                       
                    });
                    authorPage++;
                    $authorButton.text(text);
                    

                    if (len%4 != 0 && type == 'search') {
                        $authorButton.remove();
                    }

                }

                    
            
            
        });
    });
});





$(document).ready(function() {
    var catPage = 2;
    var $catButton = $('#loadCategory');
    $catButton.on('click', function() {
        var text = $catButton.text();
        var catID = $(this).data('id');
        $catButton.text('Загружаем...');
        $.post(ajaxUrl, {
            action: 'cat_ajax',
            page: catPage,
            id: catID
        }).success(function(posts) {
                var html = $.parseHTML(posts);

                if (posts == 'end') {
                    $catButton.remove();
                 
                }

                else {
                    $('#loadHere').append(posts);
                    $('#loadHere').imagesLoaded( function() {
                        $('.js-height').each(function () {
                            var id = $(this).attr('data-height');
                            if ($.inArray(id, ids)) {
                                ids[ids.length] = id;
                            }
                        });
                        blockHeight(ids);
                    });
                    catPage++;
                    $catButton.text(text);

                    
                }

                $('.js-height').each(function () {
                            var id = $(this).attr('data-height');
                            if ($.inArray(id, ids)) {
                                ids[ids.length] = id;
                            }
                        });
                        blockHeight(ids);
            
            
        });
    });
});

$(document).ready(function() {
    $('.js-submit').click(function() {
        $(this).closest('form').submit();
    });
});

$(document).ready(function() {
    $('#topBanner').addClass('topBanner');
});

$(document).ready(function() {

    function loadNextPost(a) {
        var startPost = $('.l-wrap--article').eq(0).data('post');
        $.post(ajaxUrl,{
            action: 'loadNextPost',
            article: a,
            startPost: startPost
        }).success(function(data) {
            if (data) {
                articlesArray[a] = data.data;
                $('#forNextPosts').append(data.html);
                $('#forNextPosts').imagesLoaded( function() {
                    blockHeight(ids);
                });
            }        
        });
    } 

    function loadStartPost() {
        var startPost = $('.l-wrap--current').data('post');
        $.post(ajaxUrl,{
            action: 'loadStartPostData',
            startPost: startPost
        }).success(function(data) {
            articlesArray[0] = data;
        });
    }

    function getLastPosition() {
         var last = $('.l-wrap--current .js-last').offset().top - 400;
         return last;
    }

    function getBeforeLastPosition() {
        var index = $('.l-wrap--current').index('.l-wrap--article');
        var last = $('.l-wrap--article').eq(index-1).find('.js-last').offset().top;
        return last;
    }

    function makeCurrent(art) {
        $('.l-wrap--current').removeClass('l-wrap--current');
        $('.l-wrap--article').eq(art).addClass('l-wrap--current');

        $('#header-title').text(articlesArray[art].title);
        $('#og_title').attr('content', articlesArray[art].title);

        $('#header-description').attr('content', articlesArray[art].subheading);
        $('#og_description').attr('content', articlesArray[art].subheading);

        $('#og_url').attr('content', articlesArray[art].permalink);

        $('#og_image').attr('content', articlesArray[art].opengraph_image);
        



       

        history.pushState('', articlesArray[art].title, articlesArray[art].permalink);

        var shareIDs = ['shareTop', 'shareBottom'];
        $(shareIDs).each(function(i, item){
            Ya.share2(item+art, { 
                hooks: {
                    onready: function () {
                        this.updateContent({ 
                            title: articlesArray[art].title,
                            description: articlesArray[art].subheading,
                            url: articlesArray[art].permalink,
                            image: articlesArray[art].opengraph_image
                        });
                    }
                }
             });
        });

        CheckAdBlock(); 
        
    }

    if ($('.l-wrap--current').length > 0) {
        var windowHeight = $(window).height();
       
        var startOpacity = 0.9;
        var previousScroll = 0;
        var article = 0;
        var articlesArray = [];
        var loadingProcess = 0;
        var loadStartPostData = 0;

        $('.b-foot').css('margin-top', '0');
        $('.b-foot').css('position', 'relative');
        $('.b-foot').css('z-index', '1');

        $(window).scroll(function() {
            var scroller = $(this).scrollTop();

            if (!loadStartPostData) {
                loadStartPost();
                loadStartPostData = 1;
            }

            if (!loadingProcess && loadStartPostData) {
                if (! articlesArray[article+1]) {
                   loadNextPost(article+1); 
                }
                
                loadingProcess = 1;
            }


            
            
            if (scroller+windowHeight > getLastPosition()) {
                var newOpacity = (scroller-getLastPosition())/windowHeight+0.3;

                if (startOpacity-newOpacity >= 0) {
                    $('.loading-shadow').eq(article).css('opacity', startOpacity-newOpacity);
                    $('.loading').eq(article).removeClass('opacity');
                }

                if ( previousScroll < scroller && startOpacity-newOpacity < 0 && !$('.loading').eq(article).hasClass('showed') ) {
                    $('.loading-shadow').eq(article).hide();
                    $('.loading').eq(article).addClass('showed');
                    article++;
                    makeCurrent(article);
                    loadingProcess = 0;
                }

            }

            if (previousScroll > scroller ) {
                if ($('.loading').eq(article-1).hasClass('showed') && scroller < getBeforeLastPosition()) {
                    article--;

                    var newOpacity = (scroller-getBeforeLastPosition())/windowHeight+0.3;

                    if (startOpacity-newOpacity >= 0) {
                        $('.loading-shadow').eq(article).css('opacity', startOpacity-newOpacity);
                    }
                    
                    $('.loading-shadow').eq(article).show();
                    $('.loading').eq(article).removeClass('showed');

                    makeCurrent(article);

                    $('.loading').eq(article+1).addClass('opacity');
                }
            }

            previousScroll = scroller;

        });





    }
});




















var checkHttp = function(callback) {
    var check = {
        readyStates: [false, false, false, false],
        status: null,
    };
    var respond = function(responseForce) {
        if(callback !== null) {
            if(responseForce !== undefined) {
                callback(responseForce);
                callback = null;
            } else {
                if(check.status === 0) {
                    callback(true);
                    callback = null;
                    return;
                }
                for(var i=0; i<4; i++) {
                    if(check.readyStates[i] === false) {
                        callback(true);
                        callback = null;
                        return;
                    }
                }
                callback(false);
            }
        }
    };
    
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        check.readyStates[xmlHttp.readyState-1] = true;
        check.status = xmlHttp.status;
        if(xmlHttp.readyState === 4) {
            respond();
        }
    };
    try {
        xmlHttp.open('GET', '/ad/300x250/ads/?r='+Math.round(Math.random()*100000000)+'&adsize=300x250&ad_number='+Math.round(Math.random()*100000000)+'&adname=top', true);
        xmlHttp.send();
    } catch(e) {
        if(e.result == '2153644038') {
            respond(true);
        }
    }
};


function CheckAdBlock() {
    checkHttp(function(enable){
        if (enable) {
            $('.b-list__banner, .b-news__banner, .b-category__top__banner, .b-article__banner, .b-section__banner').addClass('fuckAdBlock');

        }
    });
}



$(document).ready(function() {
    CheckAdBlock(); 
});

function smallFacts(width) {
    var startWidth = 739;

    var c = 3;

    if (width < 370) { c = 2; }


    var startHeightQuote = 330;
    var heightQuote = (startWidth-width)/c+startHeightQuote;

    var heightNum = 3*73900/width;

    return {
        'quote': width+'/'+heightQuote,
        'number': width+'/'+heightNum,
    }
}


function fotoramaFacts() {
    if ( $('.fotorama_fact').length > 0 ) {


        $('.fotorama_fact').each(function(i,element){
            var type = $(element).data('fact_type');
            var $fact = $(element).find('.fotorama__active .fact').first();

            var width = parseInt($fact.width());

            if (window.innerWidth < 740) { var c = 40; }
            else if (type == 'number' && ! $fact.hasClass('fact_link')) { var c = 20; }
            else if (type == 'quote' && $fact.hasClass('fact_link')) { var c = -20; }
            else { var c = 0; }


            var height = parseInt($fact.height()) + c;

            var size = width+'/'+height;

            $(element).fotorama({
                ratio: size
            });
        });



        
    }
}


$(document).ready(function() {
    replaceSVG();
});

$(window).resize(function() {
    fotoramaFacts();
    replaceSVG();
});

$(document).ready(function(){
    $('.fotorama_fact').on('fotorama:show', function() {
        replaceSVG();
    });

    $('.fotorama_fact').on('fotorama:showend', function() {
        fotoramaFacts();
    });

    $('.fotorama_fact').on('fotorama:ready', function() {
        fotoramaFacts();
        replaceSVG();
    });

    $('.fact_link').on('click', function() {
        location.href = $(this).data('href');
    });
    
});


$(document).ready(function() {
    var page = 1;
    
    $('#loadMainPage').on('click',function() {
        var exclude = $(this).data('exclude');
        var text = $(this).html();
        
        $(this).text('Загружаем...');
        $.post(ajaxUrl, {
            action: 'mainpage_ajax',
            page: page,
            exclude: exclude
        }).success(function(posts) {
            
            $('#loadHere').append(posts);
            page++;
            $('#loadMainPage').html(text);


        });
    });


});


$(document).ready(function() {

    $('body').on('mouseenter','.b-list__section__items__one__link', function() {
        
        $(this).closest('.b-list__section__items__one').find('.b-list__section__items__one__link').addClass('hover');
        
    });

    $('body').on('mouseleave','.b-list__section__items__one__link', function() {
        $(this).closest('.b-list__section__items__one').find('.b-list__section__items__one__link').removeClass('hover');
    });


    $('.b-category__top__item a').mouseenter( function() {
        var link = $(this).attr('href');
        $('.b-category__top__item a[href="'+link+'"]').addClass('hover');
        
    });

    $('.b-category__top__item a').mouseleave( function() {
        var link = $(this).attr('href');
        $('.b-category__top__item a[href="'+link+'"]').removeClass('hover');
        
    });

    



});

$(document).ready(function() {
    $('.b-article__image_video').each(function(i,elem) {
        var iframe = $(elem).find('iframe');
        if (iframe.attr('src').indexOf('youtube.com') > 0) {
            iframe.removeAttr('width').removeAttr('height');
            var cloned = iframe.clone();
            iframe.remove();

            var container = $('<div></div>'); 

            var html = container.append(cloned).html();

            $(elem).prepend('<div class="b-article__image__youtube"><img class="b-article__image__ratio" src="http://placehold.it/16x9"/>'+html+'</div>')

        }
       
    });
});

$(document).ready(function() {
    $('.b-article__image').each(function(i,item) {
        var image = $(item).find('img').first();

        if (image.height() > image.width()) {
            $(item).addClass('b-article__image--vertical');
        }
    });
});




