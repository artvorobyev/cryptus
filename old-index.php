<?php get_header();?>
<?php $exclude = array();?>


        <div class="b-items">

        <?php
            $main = get_posts(array('posts_per_page'=>4, 'meta_query' => array(array('key' => 'is_main','value' => '1'))));
            foreach ($main as $mainKey => $mainItem):
        ?>
            <?php if ($mainKey == 0):?>
            <div class="b-category__top bd">
                <div class="b-category__top__banner">
                   <?=get_banner_by_slug('index','1v')?>
                </div>




                <div class="b-category__top__item">
                    <a href="<?=get_permalink($mainItem)?>">
                        <span class="b-category__top__item__image">
                            <img src="<?=get_thumb_url('cryptusBig', $mainItem)?>" alt="<?=get_thumb_caption($mainItem)?>" title="" width="860" height="573">
                            <span class="b-category__top__item__image__label ff-ss">Главное</span>
                        </span>
                        <span class="b-category__top__item__text">
                            <span class="b-category__top__item__text__title ff-ss"><?=$mainItem->post_title?></span>
                            <?=get_subheading($mainItem->ID)?>
                            <span class="b-category__top__item__text__date date ff-ss"><?=get_norm_date(false,$mainItem)?></span>
                        </span>
                    </a>
                </div>
                <div class="br"></div>
            </div>
            <?php else:?>
            <?php if ($mainKey == 1 ) {$class='b-items__one_l pad-r'; $spanClass='';}?>
            <?php if ($mainKey == 2 ) {$class='b-items__one_2'; $spanClass=' b-items__one__title_big ';}?>
            <?php if ($mainKey == 3 ) {$class='pad-l'; $spanClass='';}?>
            <a class="b-items__one <?=$class?> js-height" data-height="items01" href="<?=get_permalink($mainItem)?>">
                <span class="b-items__one__title <?=$spanClass?> ff-ss"><?=$mainItem->post_title?></span>
                <?=get_subheading($mainItem->ID)?>
                <span class="b-items__one__date date ff-ss"><?=get_norm_date(false,$mainItem)?></span>
            </a>
           
        <?php endif; $exclude[] = $mainItem->ID; endforeach;?>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <div class="b-sections bd">
            <div class="b-sections__left">
                <div class="b-section b-section_smalltitle nobd">
                    <div class="b-section__title ff-ss"><a href="/today/events">Коротко</a></div>

                    <?php
                        $news = get_posts(array('posts_per_page'=>5, 'category_name' => 'events', 'post__not_in' => $exclude ));
                        $tab2[0] = 'b-section__one_tab2';
                        foreach ($news as $newsKey => $newsItem):
                            if (get_post_meta($newsItem->ID, 'is_selected', true)) {$class=' b-section__one_thing ';}
                            else $class = '';

                            if (in_array($newsKey, array(1,3,4))) $left[$newsKey] = ' tab-pad-l ';
                            if (in_array($newsKey, array(0,3,2))) $right[$newsKey] = ' tab-pad-r ';

                    ?>



                         <a class="b-section__one <?=$tab2[$newsKey].$class.$left[$newsKey].$right[$newsKey]?> nopad js-height" data-height="section12" href="<?=get_permalink($newsItem)?>">
                            <?php if(has_post_thumbnail($newsItem) && in_array($newsKey, array(2,4))):?>
                                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall', $newsItem)?>" alt="<?=get_thumb_caption($newsItem)?>" title="" width="270" height="178"></span>
                            <?php endif;?>
                            <span class="b-section__one__title ff-ss"><?=$newsItem->post_title?></span>
                            <span class="b-section__one__date <?=$left[$newsKey]?> date ff-ss"><?=get_norm_date(false,$newsItem)?>, <?=get_post_time( 'G:i', false, $newsItem )?></span>
                        </a>


                    <?php $exclude[] = $newsItem->ID; endforeach;?>
                    <div class="br"></div>
                </div>
            </div>
            <div class="b-sections__right">
                <div class="b-section bd tab-nobd">
                    <div class="b-section__title ff-ss"><a href="/today/real">На самом деле</a></div>

                    <?php
                        $real = get_posts(array('posts_per_page'=>5, 'category_name' => 'real', 'post__not_in' => $exclude ));
                        $rClass = array('', 'tab-nopad tab-pad-r', 'pad-l', 'nobd pc', 'nobd pad-l pc');
                        foreach ($real as $realKey => $realItem):
                            if ($realKey == 0):
                    ?>

                    <a class="b-section__one b-section__one_v nobd pad-r" href="<?=get_permalink($realItem)?>">
                        <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall', $realItem)?>" alt="<?=get_thumb_caption($realItem)?>" title="" width="270" height="178"></span>
                        <span class="b-section__one_v__text">
                            <span class="b-section__one__title ff-ss"><?=$realItem->post_title?></span>
                            <?=get_subheading($realItem->ID)?>
                            <span class="b-section__one__date date ff-ss"><?=get_norm_date(false,$realItem)?></span>
                        </span>
                    </a>

                    <?php else:?>

                    <a class="b-section__one <?=$rClass[$realKey]?> js-height" data-height="section01" href="<?=get_permalink($realItem)?>">
                        <span class="b-section__one__image tab mob"><img src="<?=get_thumb_url('cryptusHuge', $realItem)?>" alt="<?=get_thumb_caption($realItem)?>" title="" width="270" height="178"></span>
                        <span class="b-section__one__title ff-ss"><?=$realItem->post_title?></span>
                        <?=get_subheading($realItem->ID)?>
                        <span class="b-section__one__date pad-l date ff-ss"><?=get_norm_date(false,$realItem)?></span>
                    </a>

                    

                    <?php endif; $exclude[] = $realItem->ID; endforeach;?>

                    <div class="br"></div>
                </div>

                <div class="spacer"></div>

                <div class="b-section nobd bd bd-221">
                    <div class="b-section__title ff-ss"><a href="/life/columns">Авторские колонки</a></div>

                    <?php
                        $col = get_posts(array('posts_per_page'=>2, 'category_name' => 'columns', 'post__not_in' => $exclude ));
                        $cClass = array('pad-r', 'pad-l');
                        foreach ($col as $colKey => $colItem):
                    ?>

                    <a class="b-section__one <?=$cClass[$colKey]?> b-section__one_2 js-height" data-height="section03" href="<?=get_permalink($colItem)?>">
                        <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusMedium', $colItem)?>" alt="<?=get_thumb_caption($colItem)?>" title="" width="270" height="178"></span>
                        <span class="b-section__one__author ff-ss"><?php the_author_meta( 'display_name' , $colItem->post_author ); ?> <span class="b-section__one__author__photo"><img src="<?=get_author_photo($colItem->post_author)?>" alt="" title="" width="60" height="60"></span></span>
                        <span class="b-section__one__title ff-ss"><?=$colItem->post_title?></span>
                        <?=get_subheading($colItem->ID)?>
                        <span class="b-section__one__date <?=$cClass[$colKey]?> date ff-ss"><?=get_norm_date(false,$colItem)?></span>
                    </a>


                <?php $exclude[] = $colItem->ID; endforeach;?>

                    <div class="br"></div>
                </div>
            </div>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <?php if (get_banner_by_slug('index','2h')):?>

        <div class="banner">
            <?=get_banner_by_slug('index','2h')?>
        </div>

        <div class="spacer"></div>

        <?php endif;?>

        <div class="b-section nobd bd bd-221">
            <div class="b-section__title ff-ss"><a href="/today/figures">Цифры</a></div>
            
            <?php
                $f = get_posts(array('posts_per_page'=>3, 'category_name' => 'figures', 'post__not_in' => $exclude ));
                $fClass = array('pad-r b-section__one_2', 'b-section__one_2', 'pad-l');
                $fDateClass = array('', 'pad-l', 'pad-l');
                foreach ($f as $fKey => $fItem):
            ?>


            <a class="b-section__one <?=$fClass[$fKey]?> js-height" data-height="section03" href="<?=get_permalink($fItem)?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusMedium', $fItem)?>" alt="<?=get_thumb_caption($fItem)?>" title="" width="270" height="178"></span>
                <span class="b-section__one__title ff-ss"><?=$fItem->post_title?></span>
                <?=get_subheading($fItem->ID)?>
                <span class="b-section__one__date <?=$fDateClass[$fKey]?> date ff-ss"><?=get_norm_date(false,$fItem)?></span>
            </a>

            <?php $exclude[] = $fItem->ID; endforeach;?>

            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <div class="b-section bd bd-trans nobd">
            <div class="b-section__title ff-ss"><a href="/today/panoptikum">Паноптикум</a></div>
            
            <div class="b-section__banner">
                <?=get_banner_by_slug('index','2v')?>
            </div>

            <div class="b-section__ov">

                <?php
                    $p = get_posts(array('posts_per_page'=>4, 'category_name' => 'panoptikum', 'post__not_in' => $exclude ));
                    $pClass = array('b-section__one_trans nopad pc-pad-r tab-nopad', 'b-section__one_trans-other pad-r pc-pad-l tab-nopad tab-pad-r', 'b-section__one_trans-other pc-pad-r tab-pad-l', 'b-section__one_trans-other pad-l');
                    $pDateClass = array('pad-l pc-pad-r tab-pad-l', 'pc-pad-l tab-nopad date', 'pad-l pc-pad-r tab-nopad tab-pad-l', 'pad-l');
                    foreach ($p as $pKey => $pItem):
                ?>

                <a class="b-section__one <?=$pClass[$pKey]?> js-height" data-height="section05" href="<?=get_permalink($pItem)?>">
                    <span class="b-section__one__image">
                    <?php if ($pKey == 0):?>
                        <img src="<?=get_thumb_url('cryptusBig', $pItem)?>" alt="<?=get_thumb_caption($pItem)?>" title="" width="270" height="178">
                    <?php else:?>
                        <img src="<?=get_thumb_url('cryptusSmall', $pItem)?>" alt="<?=get_thumb_caption($pItem)?>" title="" width="270" height="178">
                    <?php endif;?>

                    <?php if (intval(get_post_meta($post->ID, 'is_partner', true))):?>
                        <span class="b-section__one__image__label b-section__one__image__label_alt ff-ss">Партнерский материал</span>
                    <?php endif;?>

                    </span>
                    <?php if ($pKey == 0):?>
                    <span class="b-section__one__text">
                    <?php endif;?>
                        <span class="b-section__one__title ff-ss"><?=$pItem->post_title?></span>
                        <?=get_subheading($pItem->ID)?>
                        <span class="b-section__one__date <?=$pDateClass[$pKey]?> date ff-ss"><?=get_norm_date(false,$pItem)?></span>
                    <?php if ($pKey == 0):?>
                    </span>
                    <?php endif;?>
                </a>
                <?php $exclude[] = $pItem->ID; endforeach;?>

            </div>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <div class="b-section bd bd-1111 bd-ov nobd">
            <div class="bd-1111__bd"></div>
            <div class="b-section__title ff-ss"><a href="/life/stories">Истории</a></div>
            <?php
            $stories = get_posts(array('posts_per_page'=>5, 'category_name' => 'stories', 'post__not_in' => $exclude ));
            $storiesClass = array('', 'pad-r', '', '', 'pad-l');
            $storiesDateClass = array('', '', 'pad-l', 'pad-l', 'pad-l');
            foreach ($stories as $storiesKey => $storiesItem):
                if ($storiesKey == 0):
            ?>
                <a class="b-section__one b-section__one_wide nopad" href="<?=get_permalink($storiesItem)?>">
                    <span class="b-section__one__image">
                        <img src="<?=get_thumb_url('cryptusBig', $storiesItem)?>" alt="<?=get_thumb_caption($storiesItem)?>" title="" width="270" height="178">
                    </span>
                    <span class="b-section__one__text">
                        <span class="b-section__one__title ff-ss"><?=$storiesItem->post_title?></span>
                        <?=get_subheading($storiesItem->ID)?>
                        <span class="b-section__one__date pad-l date ff-ss"><?=get_norm_date(false,$storiesItem)?></span>
                    </span>
                </a>
                <div class="br"></div>

                <?php else:?>

                <a class="b-section__one b-section__one_1111 <?=$storiesClass[$storiesKey]?> js-height" data-height="section06" href="<?=get_permalink($storiesItem)?>">

                    <span class="b-section__one__title ff-ss"><?=$storiesItem->post_title?></span>
                    <?=get_subheading($storiesItem->ID)?>
                    <span class="b-section__one__date <?=$storiesDateClass[$storiesKey]?> date ff-ss"><?=get_norm_date(false,$storiesItem)?></span>
                </a>
                
                <?php endif;?>
                <?php $exclude[] = $storiesItem->ID; endforeach;?>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <div class="b-section bd bd-trans nobd">
            <div class="b-section__title ff-ss"><a href="/life/faq">F.A.Q.</a></div>
            <div class="b-section__banner">
                <?=get_banner_by_slug('index','3v')?>
                
            </div>
            <div class="b-section__ov">
                <?php
                    $faq = get_posts(array('posts_per_page'=>4, 'category_name' => 'faq', 'post__not_in' => $exclude ));
                    $faqClass = array('b-section__one_trans nopad pc-pad-r tab-nopad', 'b-section__one_trans-other pad-r pc-pad-l tab-nopad tab-pad-r', 'b-section__one_trans-other pc-pad-r tab-pad-l', 'b-section__one_trans-other pad-l');
                    $faqDateClass = array('pad-l pc-pad-r tab-pad-l', 'pc-pad-l tab-nopad date', 'pad-l pc-pad-r tab-nopad tab-pad-l', 'pad-l');
                    foreach ($faq as $faqKey => $faqItem):
                ?>

                <a class="b-section__one <?=$faqClass[$faqKey]?> js-height" data-height="section05" href="<?=get_permalink($faqItem)?>">
                    <span class="b-section__one__image">
                    <?php if ($faqKey == 0):?>
                        <img src="<?=get_thumb_url('cryptusBig', $faqItem)?>" alt="<?=get_thumb_caption($faqItem)?>" title="" width="270" height="178">
                    <?php else:?>
                        <img src="<?=get_thumb_url('cryptusSmall', $faqItem)?>" alt="<?=get_thumb_caption($faqItem)?>" title="" width="270" height="178">
                    <?php endif;?>
                    <?php if (intval(get_post_meta($post->ID, 'is_partner', true))):?>
                        <span class="b-section__one__image__label b-section__one__image__label_alt ff-ss">Партнерский материал</span>
                    <?php endif;?>
                    </span>
                    <?php if ($faqKey == 0):?>
                    <span class="b-section__one__text">
                    <?php endif;?>
                        <span class="b-section__one__title ff-ss"><?=$faqItem->post_title?></span>
                        <?=get_subheading($faqItem->ID)?>
                        <span class="b-section__one__date <?=$faqDateClass[$faqKey]?> date ff-ss"><?=get_norm_date(false,$faqItem)?></span>
                    <?php if ($faqKey == 0):?>
                    </span>
                    <?php endif;?>
                </a>
                <?php $exclude[] = $faqItem->ID; endforeach;?>
            </div>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <div class="b-section bd bd-1111 bd-tab50 nobd">
            <div class="bd-1111__bd"></div>
            <div class="b-section__title ff-ss"><a href="/life/nature">Природа</a></div>
            <?php
            $nature = get_posts(array('posts_per_page'=>4, 'category_name' => 'nature', 'post__not_in' => $exclude ));
            $natureClass = array('pad-r', 'tab-nopad tab-pad-l', 'tab-nopad tab-pad-r', 'pad-l');
            $natureDateClass = array('', 'pad-l', 'pad-l tab-nopad', 'pad-l');
            foreach ($nature as $natureKey => $natureItem):
                ?>
            <a class="b-section__one b-section__one_tab3 <?=$natureClass[$natureKey]?> js-height" data-height="section08" href="<?=get_permalink($natureItem)?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall', $natureItem)?>" alt="<?=get_thumb_caption($natureItem)?>" title="" width="270" height="178"></span>
                <span class="b-section__one__title ff-ss"><?=$natureItem->post_title?></span>
                <?=get_subheading($natureItem->ID)?>
                <span class="b-section__one__date date <?=$natureDateClass[$natureKey]?> ff-ss"><?=get_norm_date(false,$natureItem)?></span>
            </a>
            
            <?php $exclude[] = $natureItem->ID; endforeach;?>

            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <?php if (get_banner_by_slug('index','3h')):?>

        <div class="banner">
            <?=get_banner_by_slug('index','3h')?>
            
        </div>

        <div class="spacer"></div>

        <?php endif;?>

        <div class="b-section bd bd-50 nobd">
            <div class="b-section__title ff-ss"><a href="/life/play">PLAY</a></div>
            <?php
            $play = get_posts(array('posts_per_page'=>2, 'category_name' => 'play', 'post__not_in' => $exclude ));
            $playClass = array('pad-r', 'pad-l');
            $playDateClass = array('', 'pad-l');
            foreach ($play as $playKey => $playItem):
                ?>
            <a class="b-section__one b-section__one_50 <?=$playClass[$playKey]?> js-height" data-height="section09" href="<?=get_permalink($playItem)?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusMedium', $playItem)?>" alt="<?=get_thumb_caption($playItem)?>" title="" width="270" height="178"></span>
                <span class="b-section__one__title ff-ss"><?=$playItem->post_title?></span>
                <span class="b-section__one__date <?=$playDateClass[$playKey]?> date ff-ss"><?=get_norm_date(false,$playItem)?></span>
            </a>


            <?php $exclude[] = $playItem->ID; endforeach;?>

            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <?php

            $images = explode(',', get_option('all_gallery_images'));
            $count = count($images);

            if ($count):

        ?>

        <div class="b-section nobd">
            <div class="b-section__title ff-ss"><a>Кадры</a></div>
            <div class="b-section__flex">
                <div class="b-section__gallery b-section__gallery--fotorama">
                    <div class="b-section__gallery__wrap">
                        
                        <div class="fotorama" data-width="920" data-maxwidth="100%" data-ratio="920/613" data-maxheight="613" data-margin="0" data-nav="thumbs" data-thumbheight="42">
                            <?php
                                foreach ($images as $i=>$image) :

                                    $url[$i] = wp_get_attachment_image_src($image, 'cryptusHuge');
                                    $meta[$i] = wp_prepare_attachment_for_js($image);

                                    $caption[$i] = htmlspecialchars($meta[$i]['caption']);
                                    $descr[$i] = htmlspecialchars($meta[$i]['description']);


                                    if (!$descr[$i]) $descr[$i] = '&nbsp;'

                            ?>
                                    <img src="<?=$url[$i][0]?>" alt="" title="" width="700" height="467" data-cap="<?=$caption[$i]?>" data-description="<?=$descr[$i]?>">

                            <?php endforeach;?>
                        </div>
                        
                    </div>
                </div>
                <div class="b-section__text b-section__text--fotorama ff-ss nobd">
                    <div class='b-section__text__descr b-section__text__descr--fotorama js-fotorama-caption'>
                        <?=$caption[0]?>
                    </div>
                </div>

            </div>
            <div class='b-section__text__author b-section__text__author--fotorama ff-ss js-fotorama-description'>
                <?=$descr[0]?>
            </div>
        </div>

        <div class="br" style="border-bottom: 1px solid #bdc3cc;"></div>

        <div class="spacer"></div>

        <?php endif;?>

        <div class="b-section bd bd-trans nobd">
            <div class="b-section__title ff-ss"><a href="/sandbox">Песочница</a></div>
            <div class="b-section__banner">
                 <?=get_banner_by_slug('index','4v')?>
               
            </div>
            <div class="b-section__ov">
                <?php
                    $sandbox = get_posts(array('posts_per_page'=>4, 'category_name' => 'sandbox', 'post__not_in' => $exclude ));
                    $sandboxClass = array('b-section__one_trans nopad pc-pad-r tab-nopad', 'b-section__one_trans-other pad-r pc-pad-l tab-nopad tab-pad-r', 'b-section__one_trans-other pc-pad-r tab-pad-l', 'b-section__one_trans-other pad-l');
                    $sandboxDateClass = array('pad-l pc-pad-r tab-pad-l', 'pc-pad-l tab-nopad date', 'pad-l pc-pad-r tab-nopad tab-pad-l', 'pad-l');
                    foreach ($sandbox as $sandboxKey => $sandboxItem):
                ?>

                <a class="b-section__one <?=$sandboxClass[$sandboxKey]?> js-height" data-height="section05" href="<?=get_permalink($sandboxItem)?>">
                    <span class="b-section__one__image">
                    <?php if ($sandboxKey == 0):?>
                        <img src="<?=get_thumb_url('cryptusBig', $sandboxItem)?>" alt="<?=get_thumb_caption($sandboxItem)?>" title="" width="270" height="178">
                    <?php else:?>
                        <img src="<?=get_thumb_url('cryptusSmall', $sandboxItem)?>" alt="<?=get_thumb_caption($sandboxItem)?>" title="" width="270" height="178">
                    <?php endif;?>
                    <?php if (intval(get_post_meta($post->ID, 'is_partner', true))):?>
                        <span class="b-section__one__image__label b-section__one__image__label_alt ff-ss">Партнерский материал</span>
                    <?php endif;?>
                    </span>
                    <?php if ($sandboxKey == 0):?>
                    <span class="b-section__one__text">
                    <?php endif;?>
                        <span class="b-section__one__title ff-ss"><?=$sandboxItem->post_title?></span>
                        <?=get_subheading($sandboxItem->ID)?>
                        <span class="b-section__one__date <?=$sandboxDateClass[$sandboxKey]?> date ff-ss"><?=get_norm_date(false,$sandboxItem)?></span>
                    <?php if ($sandboxKey == 0):?>
                    </span>
                    <?php endif;?>
                </a>
                <?php $exclude[] = $sandboxItem->ID; endforeach;?>
            </div>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <div class="b-sections-nav ff-ss">
            <div class="b-section b-section_25 bd">
                <div class="b-section__title"><a href="/technology">Технологии</a></div>
                <?php
                $tech = get_posts(array('posts_per_page'=>4, 'category_name' => 'technology', 'post__not_in' => $exclude ));
                foreach ($tech as $techKey => $techItem):
                ?>
                <a class="b-section__one" href="<?=get_permalink($techItem)?>">
                    <span class="b-section__one__title"><?=$techItem->post_title?></span>
                    <span class="b-section__one__date date"><?=get_norm_date(false,$techItem)?></span>
                </a>
                <?php $exclude[] = $techItem->ID; endforeach;?>
                <div class="br"></div>
            </div>
            <div class="b-section bd">
                <div class="b-section__title"><a href="/medicine">Медицина</a></div>
                <?php
                $med = get_posts(array('posts_per_page'=>4, 'category_name' => 'medicine', 'post__not_in' => $exclude ));
                foreach ($med as $medKey => $medItem):
                ?>
                <a class="b-section__one" href="<?=get_permalink($medItem)?>">
                    <span class="b-section__one__title"><?=$medItem->post_title?></span>
                    <span class="b-section__one__date date"><?=get_norm_date(false,$medItem)?></span>
                </a>
                <?php $exclude[] = $medItem->ID; endforeach;?>
                <div class="br"></div>
            </div>
            <div class="b-section b-section_15 bd">
                <div class="b-section__title"><a href="/life/nature">Природа</a></div>
                <?php
                $nat = get_posts(array('posts_per_page'=>4, 'category_name' => 'sandbox', 'post__not_in' => $exclude ));
                $natClass = array('tab-pad-r', 'tab-pad-l tab-pad-r', 'tab-pad-l tab-pad-r', 'tab-pad-l');
                $natDateClass = array('', 'tab-pad-l', 'tab-pad-l', 'tab-pad-l');
                foreach ($nat as $natKey => $natItem):
                ?>
                <a class="b-section__one <?=$natClass[$natItem]?> js-height" data-height="section11" href="#">
                    <span class="b-section__one__title"><?=$natItem->post_title?></span>
                    <span class="b-section__one__date <?=$natDateClass[$natItem]?> date"><?=get_norm_date(false,$natItem)?></span>
                </a>
                
                 <?php $exclude[] = $natItem->ID; endforeach;?>
                <div class="br"></div>
            </div>
            <div class="br"></div>
        </div>

        <div class="spacer"></div>

        <?php if (get_banner_by_slug('index','4h')):?>

        <div class="banner">
         <?=get_banner_by_slug('index','4h')?>

        </div>

        <div class="spacer"></div>
        <?php endif;?>
    </div>

<?php get_footer();?>