<?php get_header();?>

        <div class="b-list ff-ss">
            <div class="b-list__banner">
            <?=get_banner_by_slug('event_archive','1v')?>
            </div>
            <div class="b-list__view">
                <div class="b-list__title"><h1>События</h1></div>

                <?php
                    $today = new DateTime(date('d.m.Y'));
                    $yesterday = new DateTime(date('d.m.Y'));
                    $yesterday->modify('-1 day');

                    echo get_news($today);
                    echo get_news($yesterday);
                ?>


                <div id="newsLoad"></div>
                
                <div class="br"></div>

                <div class="spacer"></div>

                <a href="javascript:void(0)" class="btn" id="newsMore">Загрузить ещё</a>

                <div class="spacer"></div>
            </div>
        </div>
        <div class="spacer" style="clear:both"></div>

        <div class="banner">
         <?=get_banner_by_slug('event_archive','2h')?>

        </div>

        <div class="spacer"></div>
    </div>

<?php get_footer();?>