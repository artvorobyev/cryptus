<?php get_header();?>
        <div class="b-404">
            <div class="b-404__title ff-ss">Страница не найдена</div>
            <div class="b-404__text">
                Возможно, эта страница удалена, переименована или просто временно недоступна.<br>
                Попробуйте посмотреть на <a href="/" class="td-u"><span>главной странице</span></a>
            </div>
            <div class="b-404__404 ff-ss">404</div>
        </div>
    </div>

<?php get_footer();?>