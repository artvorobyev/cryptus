<?php get_header(); the_post();?>

        <div class="b-news">
            <div class="b-news__view">
                <div class="b-news__title"><h1><?php the_title();?></h1></div>
                <div class="b-news__date date ff-ss"><?=get_norm_date()?>, <?=get_the_time('G:i')?></div>
                <div class="b-news__text">
                    <?php the_content();?>
                </div>

                <div class="b-article__info b-article__info_bottom b-article__info_tagged ff-ss">
                    <div class="b-article__info__left b-news__share">
                        Расскажите друзьям
                    </div>
                    <div class="b-article__info__right b-article__info__right_circles">
                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="//yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="facebook,vkontakte,twitter,odnoklassniki" data-counter=""></div>
                    </div>
                    <div class="br"></div>
                </div>
            </div>
            <div class="br"></div>
        </div>

        <div class="br"></div>

        <div class="spacer"></div>

        
    </div>

<? get_footer(); ?>