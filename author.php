<?php
get_header();
$count = 0;
global $wp_query;
$all = $wp_query->post_count;
$lost = $all%4;
$bd = array('', 'bd-noc bd-nor', 'bd-nor','');
?>

        <div class="b-title">
            <?php if (get_user_meta(get_the_author_meta('ID'), 'authorImage', true)):?>
            <div class="b-title__userpic"><img src="<?=get_author_photo(get_the_author_meta('ID'), 'authorBig')?>" alt="" title="" width="150" height="150"></div>
            <?php endif;?>
            <div class="b-title__text">
                <h1><?=str_replace(' ', '<br/>', get_the_author())?></h1>
                <div class="b-title__descr"><?php the_author_description()?></div>
            </div>
            <div class="br"></div>
        </div>

        <div class="br"></div>

        <div class="b-section bd bd-1111 bd-0 bd-tab50 nobd <?php if ($all == $lost) echo $bd[$lost]?>">
            <div class="bd-1111__bd"></div>

        <?php while(have_posts()):

                the_post();
                $count++;

                if ($count%4==1) {$class = 'pad-r'; $dateClass = '';}
                elseif ($count%4==2) {$class = 'tab-nopad tab-pad-l'; $dateClass = 'pad-l';}
                elseif ($count%4==3) {$class = 'tab-nopad tab-pad-r'; $dateClass = 'pad-l tab-nopad';}
                elseif ($count%4==0) {$class = 'pad-l'; $dateClass = 'pad-l';}
            ?>
            <a class="b-section__one b-section__one_tab3 <?=$class?> js-height" data-height="section08" href="<?php the_permalink();?>">
                <span class="b-section__one__image"><img src="<?=get_thumb_url('cryptusSmall')?>" alt="<?=get_thumb_caption()?>" title="" width="270" height="178"></span>
                <span class="b-section__one__title ff-ss"><?php the_title();?></span>
                <?=get_subheading()?>
                <span class="b-section__one__date <?=$dateClass?> date ff-ss"><?=get_norm_date(false)?></span>
            </a>

           

            <?php if ($count%4==0 || $count == $all):?>
            <div class="br"></div>
        </div>
            <?php endif;?>

        <?php if ($count%4==0 && $all > $count):?> 

        <div class="b-section bd bd-1111 bd-0 bd-tab50 nobd <?php if ($count == $all-$lost) echo $bd[$lost]?>">
            <div class="bd-1111__bd"></div>
        <?php endif;?>

    <?php endwhile;?>

    <div id="loadHere"></div>

     <div class="spacer" style="clear:both"></div>

        <?php if ($wp_query->found_posts > $count):?><a href="javascript:void(0)" class="btn" id="authorMore" data-type="author" data-id="<?=get_the_author_meta('ID')?>">Загрузить ещё</a><?php endif;?>



        <div class="spacer"></div>

        <div class="banner"><?=get_banner_by_slug('other','2h')?></div>

        <div class="spacer"></div>
    </div>

   <?php get_footer();?>