<?php get_header();?>

        <div class="b-list ff-ss">

            <div class="b-list__banner"><?=get_banner_by_slug('search','1v')?></div>


            <div class="b-list__view">
                <div class="b-list__form">
                    <form action="/" method="get">
                        <div class="b-list__form__btn"><input type="submit" value="Найти" class="btn"></div>
                        <div class="b-list__form__field b-input"><input type="text" name="s" value="<?=$_GET['s']?>"></div>
                    </form>
                </div>
                <div class="b-list__section">
                    <div class="b-list__section__items" id="loadHere">

                        <?php while(have_posts()): the_post();?>

                        <div class="b-list__section__items__one">
                            <?php if (has_post_thumbnail()):?>
                            <div class="b-list__section__items__one__image"><a href="<?php the_permalink();?>"><img src="<?=get_thumb_url('cryptusSmall')?>" alt="<?=get_thumb_caption()?>" title="" width="270" height="178"></a></div>
                            <div class="b-list__section__items__one__text">
                            <?php endif;?>
                                <a href="<?php the_permalink();?>"><?=highlight_results(get_the_title())?></a>
                                <div class="b-list__section__items__one__descr ff-s">
                                     <?=extract_search_content(get_the_content())?> 
                                </div>
                                <div class="b-list__section__items__one__date date"><?=get_norm_date(false)?></div>
                            <?php if (has_post_thumbnail()):?>
                            </div>
                             <?php endif;?>
                        </div>

                        <?php endwhile;?>

                        
                    </div>
                    <div class="br"></div>
                </div>
                <div class="br"></div>

                <div class="spacer"></div>

                 <?php if ($wp_query->found_posts > $wp_query->post_count):?><a href="javascript:void(0)" class="btn" id="authorMore" data-type="search" data-id="<?=$_GET['s']?>">Загрузить ещё</a><?php endif;?>

                <div class="spacer"></div>
            </div>
        </div>
    </div>

<?php get_footer();?>